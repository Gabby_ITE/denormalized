<?php

use App\Http\Controllers\Api\AuthorController;
use App\Http\Controllers\Api\Book\BookController;
use App\Http\Controllers\Api\Book\BookmarkController;
use App\Http\Controllers\Api\Book\BookNoteController;
use App\Http\Controllers\Api\Book\BookStatusController;
use App\Http\Controllers\Api\Challenge\ChallengeController;
use App\Http\Controllers\Api\Club\ClubController;
use App\Http\Controllers\Api\SeriesController;
use App\Http\Controllers\Api\TagController;
use App\Http\Controllers\Api\User\DiscussionController;
use App\Http\Controllers\Api\User\PostController;
use App\Http\Controllers\Api\User\RatingController;
use App\Http\Controllers\Api\User\ReactController;
use App\Http\Controllers\Api\User\RequestController;
use App\Http\Controllers\Api\User\ResponseController;
use App\Http\Controllers\Api\User\ReviewController;
use App\Http\Controllers\Api\User\UserController;
use App\Models\Book\BookStatus;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Admin
Route::middleware(['auth:sanctum', 'admin'])->group(function () {
    Route::post('authors',[AuthorController::class,'store']);
    Route::patch('authors/{author}',[AuthorController::class,'update']);
    Route::delete('authors/{author}',[AuthorController::class,'destroy']);
    Route::post('authors/{author}/update-image',[AuthorController::class,'updateImage']);

    Route::post('tags',[TagController::class,'store']);
    Route::patch('tags/{tag}',[TagController::class,'update']);
    Route::delete('tags/{tag}',[TagController::class,'destroy']);

    Route::post('series',[SeriesController::class,'store']);
    Route::post('series/{serie}',[SeriesController::class,'update']);
    Route::post('series/{serie}/add-tags',[SeriesController::class,'addTags']);
    Route::post('series/{serie}/add-authors',[SeriesController::class,'addAuthors']);
    Route::post('series/{serie}/add-descriptions',[SeriesController::class,'addDescriptions']);
    Route::post('series/{serie}/add-books',[SeriesController::class,'addBooks']);
    Route::post('series/{serie}/delete-tags',[SeriesController::class,'deleteTags']);
    Route::post('series/{serie}/delete-authors',[SeriesController::class,'deleteAuthors']);
    Route::post('series/{serie}/delete-descriptions',[SeriesController::class,'deleteDescriptions']);
    Route::post('series/{serie}/delete-books',[SeriesController::class,'deleteBooks']);
    Route::delete('series/{serie}',[SeriesController::class,'destroy']);
});

// User
Route::prefix('auth')->group(function () {
    Route::post('register',[UserController::class,'register']);
    Route::post('login',[UserController::class,'login']);
    Route::get('logout',[UserController::class,'logout'])->middleware('auth:sanctum');
    Route::post('register/validate',[UserController::class,'validateRegistration']);
    Route::get('reset-password',[UserController::class,'resetPassword'])->middleware('auth:sanctum');
    Route::post('reset-password/validate',[UserController::class,'validateResetPassword'])->middleware('auth:sanctum');
    Route::post('update-image',[UserController::class,'updateImage'])->middleware('auth:sanctum');
    Route::delete('delete',[UserController::class,'destroy'])->middleware('auth:sanctum');
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('users',[UserController::class,'index']);
    Route::get('users/{user}',[UserController::class,'show']);

    Route::get('authors',[AuthorController::class,'index']);
    Route::get('authors/{author}',[AuthorController::class,'show']);

    Route::get('tags',[TagController::class,'index']);
    Route::get('tags/{tag}',[TagController::class,'show']);

    // this here can both admin and user use it
    Route::get('books',[BookController::class,'index']);
    Route::get('books/{book}',[BookController::class,'show']);
    Route::post('books',[BookController::class,'store']);
    Route::post('books/{book}',[BookController::class,'update']);
    Route::get('private-books',[BookController::class,'privateBooks']);
    Route::post('books/{book}/add-tags',[BookController::class,'addTags']);
    Route::post('books/{book}/add-authors',[BookController::class,'addAuthors']);
    Route::post('books/{book}/add-descriptions',[BookController::class,'addDescriptions']);
    Route::post('books/{book}/delete-tags',[BookController::class,'deleteTags']);
    Route::post('books/{book}/delete-authors',[BookController::class,'deleteAuthors']);
    Route::post('books/{book}/delete-descriptions',[BookController::class,'deleteDescriptions']);
    Route::delete('books/{book}',[BookController::class,'destroy']);

    Route::get('series',[SeriesController::class,'index']);
    Route::get('series/{serie}',[SeriesController::class,'show']);

    Route::get('clubs',[ClubController::class,'index']);
    Route::get('clubs/{club}',[ClubController::class,'show']);
    Route::post('clubs',[ClubController::class,'store']);
    Route::post('clubs/{club}',[ClubController::class,'update']);
    Route::post('clubs/{club}/add-tags',[ClubController::class,'addTags']);
    Route::post('clubs/{club}/delete-tags',[ClubController::class,'deleteTags']);
    Route::delete('clubs/{club}',[ClubController::class,'destroy']);

    Route::get('users/{posted}/posts',[PostController::class,'index']);
    Route::get('users/{posted}/posts/{post}',[PostController::class,'show']);
    Route::post('users/{posted}/posts',[PostController::class,'store']);
    Route::post('users/{posted}/posts/{post}',[PostController::class,'update']);
    Route::post('users/{posted}/posts/{post}/add-images',[PostController::class,'addImages']);
    Route::post('users/{posted}/posts/{post}/delete-images',[PostController::class,'deleteImages']);
    Route::delete('users/{posted}/posts/{post}',[PostController::class,'destroy']);

    Route::get('clubs/{posted}/posts',[PostController::class,'index']);
    Route::get('clubs/{posted}/posts/{post}',[PostController::class,'show']);
    Route::post('clubs/{posted}/posts',[PostController::class,'store']);
    Route::post('clubs/{posted}/posts/{post}',[PostController::class,'update']);
    Route::post('clubs/{posted}/posts/{post}/add-images',[PostController::class,'addImages']);
    Route::post('clubs/{posted}/posts/{post}/delete-images',[PostController::class,'deleteImages']);
    Route::delete('clubs/{posted}/posts/{post}',[PostController::class,'destroy']);

    Route::get('books/{discussed}/discussions',[DiscussionController::class,'index']);
    Route::post('books/{discussed}/discussions',[DiscussionController::class,'store']);
    Route::put('books/{discussed}/discussions/{discussion}',[DiscussionController::class,'update']);
    Route::delete('books/{discussed}/discussions/{discussion}',[DiscussionController::class,'destroy']);

    Route::get('series/{discussed}/discussions',[DiscussionController::class,'index']);
    Route::post('series/{discussed}/discussions',[DiscussionController::class,'store']);
    Route::put('series/{discussed}/discussions/{discussion}',[DiscussionController::class,'update']);
    Route::delete('series/{discussed}/discussions/{discussion}',[DiscussionController::class,'destroy']);

    Route::get('books/{reviewed}/reviews',[ReviewController::class,'index']);
    Route::post('books/{reviewed}/reviews',[ReviewController::class,'store']);
    Route::put('books/{reviewed}/reviews/{review}',[ReviewController::class,'update']);
    Route::delete('books/{reviewed}/reviews/{review}',[ReviewController::class,'destroy']);

    Route::get('series/{reviewed}/reviews',[ReviewController::class,'index']);
    Route::post('series/{reviewed}/reviews',[ReviewController::class,'store']);
    Route::put('series/{reviewed}/reviews/{review}',[ReviewController::class,'update']);
    Route::delete('series/{reviewed}/reviews/{review}',[ReviewController::class,'destroy']);


    Route::post('books/{page}/reviews/{reacted}/react',[ReactController::class,'store']);
    Route::put('books/{page}/reviews/{reacted}/react',[ReactController::class,'update']);
    Route::delete('books/{page}/reviews/{reacted}/react',[ReactController::class,'destroy']);

    Route::post('series/{page}/reviews/{reacted}/react',[ReactController::class,'store']);
    Route::put('series/{page}/reviews/{reacted}/react',[ReactController::class,'update']);
    Route::delete('series/{page}/reviews/{reacted}/react',[ReactController::class,'destroy']);

    Route::post('books/{page}/discussions/{reacted}/react',[ReactController::class,'store']);
    Route::put('books/{page}/discussions/{reacted}/react',[ReactController::class,'update']);
    Route::delete('books/{page}/discussions/{reacted}/react',[ReactController::class,'destroy']);

    Route::post('series/{page}/discussions/{reacted}/react',[ReactController::class,'store']);
    Route::put('series/{page}/discussions/{reacted}/react',[ReactController::class,'update']);
    Route::delete('series/{page}/discussions/{reacted}/react',[ReactController::class,'destroy']);

    Route::post('users/{page}/posts/{reacted}/react',[ReactController::class,'store']);
    Route::put('users/{page}/posts/{reacted}/react',[ReactController::class,'update']);
    Route::delete('users/{page}/posts/{reacted}/react',[ReactController::class,'destroy']);

    Route::post('clubs/{page}/posts/{reacted}/react',[ReactController::class,'store']);
    Route::put('clubs/{page}/posts/{reacted}/react',[ReactController::class,'update']);
    Route::delete('clubs/{page}/posts/{reacted}/react',[ReactController::class,'destroy']);


    Route::post('books/{book}/rating',[RatingController::class,'store']);
    Route::put('books/{book}/rating',[RatingController::class,'update']);
    Route::delete('books/{book}/rating',[RatingController::class,'destroy']);

    Route::post('authors/{author}/rating',[RatingController::class,'store']);
    Route::put('authors/{author}/rating',[RatingController::class,'update']);
    Route::delete('authors/{author}/rating',[RatingController::class,'destroy']);

    Route::post('clubs/{club}/rating',[RatingController::class,'store']);
    Route::put('clubs/{club}/rating',[RatingController::class,'update']);
    Route::delete('clubs/{club}/rating',[RatingController::class,'destroy']);

    Route::post('series/{series}/rating',[RatingController::class,'store']);
    Route::put('series/{series}/rating',[RatingController::class,'update']);
    Route::delete('series/{series}/rating',[RatingController::class,'destroy']);

    Route::get('books/{book}/book-notes',[BookNoteController::class,'index']);
    Route::get('books/{book}/book-notes/{bookNote}',[BookNoteController::class,'show']);
    Route::post('books/{book}/book-notes/',[BookNoteController::class,'store']);
    Route::put('books/{book}/book-notes/{bookNote}',[BookNoteController::class,'update']);
    Route::patch('books/{book}/book-notes/{bookNote}',[BookNoteController::class,'update']);
    Route::delete('books/{book}/book-notes/{bookNote}',[BookNoteController::class,'destroy']);

    Route::get('books/{book}/bookmarks',[BookmarkController::class,'index']);
    Route::post('books/{book}/bookmarks/',[BookmarkController::class,'store']);
    Route::delete('books/{book}/bookmarks/{bookmark}',[BookmarkController::class,'destroy']);

    Route::get('requests',[RequestController::class,'index']);
    Route::get('requests/{request}',[RequestController::class,'show']);
    Route::get('reports',[RequestController::class,'reports']);
    Route::get('book_requests',[RequestController::class,'book_requests']);
    Route::get('club_requests',[RequestController::class,'club_requests']);
    Route::get('friend_requests',[RequestController::class,'friend_requests']);
    Route::post('requests',[RequestController::class,'store']);
    Route::delete('requests',[RequestController::class,'destroy']);

    Route::get('responses',[ResponseController::class,'index']);
    Route::post('requests/{userRequest}/handle',[ResponseController::class,'handle']);

    Route::get('challenges',[ChallengeController::class,'index']);
    Route::get('challenges/{challenge}',[ChallengeController::class,'show']);
    Route::post('challenges',[ChallengeController::class,'store']);
    Route::put('challenges/{challenge}',[ChallengeController::class,'update']);
    Route::delete('challenges/{challenge}',[ChallengeController::class,'destroy']);
    Route::get('progresses',[ChallengeController::class,'progresses']);
    Route::get('progresses/{challenge}/',[ChallengeController::class,'show_progress']);
    Route::post('progresses/{challenge}/',[ChallengeController::class,'take']);
    Route::delete('progresses/{challenge}/',[ChallengeController::class,'remove']);

    Route::get('statuses',[BookStatusController::class,'statuses']);
    Route::post('books/{book}/statuses',[BookStatusController::class,'store']);
    Route::put('books/{book}/statuses',[BookStatusController::class,'update']);
});

