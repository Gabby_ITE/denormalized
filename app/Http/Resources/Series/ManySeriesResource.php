<?php

namespace App\Http\Resources\Series;

use App\Models\Series\SeriesRating;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ManySeriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'cover'=>$this->cover,
            'rating'=>$this->ratings()
        ];
    }
}
