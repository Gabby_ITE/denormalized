<?php

namespace App\Http\Resources\Series;

use App\Http\Resources\Author\ManyAuthorResource;
use App\Http\Resources\AuthorResource;
use App\Http\Resources\Book\ManyBookResource;
use App\Http\Resources\Combine\DescriptionResource;
use App\Http\Resources\TagResource;
use App\Models\Series\Series;
use App\Models\Series\SeriesRating;
use App\Models\User\Rating;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleSeriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'cover'=>$this->cover,
            'release_date'=>$this->release_date,
            'user_rating'=>$this->user_rating($request),
            'ratings'=>$this->ratings(),
            'full_ratings'=>$this->full_ratings(),
            'authors'=>ManyAuthorResource::collection($this->authors()),
            'tags'=>TagResource::collection($this->tags()),
            'books'=>ManyBookResource::collection($this->books),
            'descriptions'=>DescriptionResource::collection($this->descriptions()),
        ];
    }
    private function user_rating(Request $request)
    {
       $rating = Rating::where('type','book')->where('rated_id',$this->id)
            ->where('user_id',$request->user()->id)->first();
        if ($rating){
            return $rating->select('id','rating');
        }
        return null;
    }

    private function  rating()
    {
        if ($this->hasMany(SeriesRating::class)->count()==0)
            return null;
        $ratings =  $this->hasMany(SeriesRating::class);
        if ($ratings->count() > 0){
            $sum = 0;
            foreach($ratings->select('rating')->get() as $rating){
                return $sum = $sum + $rating->rating;
            }
            $sum = $sum/$ratings->count();
            return $sum;
        }
    }

    public function hasBooks(){
        return ManyBookResource::collection(
            $this->books->pluck('book')->sortByDesc('release_date')
        );
    }
}
