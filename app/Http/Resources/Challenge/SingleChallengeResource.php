<?php

namespace App\Http\Resources\Challenge;

use App\Http\Resources\Book\ManyBookResource;
use App\Http\Resources\User\ManyUserResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleChallengeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        if ($this->book_id > 0){
            return [
                'id'=>$this->id,
                'creator'=>new ManyUserResource($this->creator),
                'book_id'=>new ManyBookResource($this->book),
                'name'=>$this->name,
                'type'=>$this->type,
                'start_date'=>$this->start_date,
                'end_date'=>$this->end_date,
                'max_number'=>$this->max_number,
                'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
            ];
        }else{
            return [
                'id'=>$this->id,
                'creator'=>new ManyUserResource($this->creator),
                'name'=>$this->name,
                'type'=>$this->type,
                'start_date'=>$this->start_date,
                'end_date'=>$this->end_date,
                'max_number'=>$this->max_number,
                'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
            ];
        }
    }
}
