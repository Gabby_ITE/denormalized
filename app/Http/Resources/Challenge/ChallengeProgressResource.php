<?php

namespace App\Http\Resources\Challenge;

use App\Http\Resources\User\ManyUserResource;
use App\Models\Challenge\Challenge;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ChallengeProgressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'challenge'=>new ManyChallengeResource($this->challenge),
            'acheived_number'=>$this->acheived_number,
            'status'=>$this->status,
            'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
        ];
    }
}
