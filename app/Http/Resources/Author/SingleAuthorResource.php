<?php

namespace App\Http\Resources\Author;

use App\Http\Resources\Book\ManyBookResource;
use App\Http\Resources\Series\ManySeriesResource;
use App\Models\User\Rating;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleAuthorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'first_name'=>$this->first_name,
            'last_name'=>$this->last_name,
            'image'=>$this->image,
            'your_rating'=>$this->user_rate($request),
            'ratings'=>$this->ratings(),
            'full_ratings'=>$this->full_ratings(),
            'books'=>ManyBookResource::collection($this->books()),
            'series'=>ManySeriesResource::collection($this->series()),
        ];
    }
    private function user_rate(Request $request)
    {
       $rating = Rating::where('type','author')->where('rated_id',$this->id)
            ->where('user_id',$request->user()->id)->first();
        if ($rating){
            return [
                'id'=>$rating->id,
                'rating'=>$rating->rating,
            ];
        }
        return null;
    }
}
