<?php

namespace App\Http\Resources\Club;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ManyClubResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return  [
            'id'=>$this->id,
            'name'=>$this->name,
            'image'=>$this->image,
            'ratings'=>$this->ratings(),
            'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
        ];
    }
}
