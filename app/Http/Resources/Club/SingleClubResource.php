<?php

namespace App\Http\Resources\Club;

use App\Http\Resources\TagResource;
use App\Http\Resources\User\ManyPostResource;
use App\Http\Resources\User\ManyUserResource;
use App\Models\User\Rating;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleClubResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        if($this->is_closed){
            if ($request->user()->role == 'admin'
                || $this->members->where('user_id',$request->user()->id)->first()){
                    return  [
                        'id'=>$this->id,
                        'name'=>$this->name,
                        'image'=>$this->image,
                        'descritpion'=>$this->description,
                        'user_rating'=>$this->user_rating($request),
                        'full_ratings'=>$this->full_ratings(),
                        'ratings'=>$this->ratings(),
                        'admin'=>new ManyUserResource($this->admin),
                        'members'=>ManyUserResource::collection($this->members->pluck('user')),
                        'number_of_members'=>$this->members->count(),
                        'posts'=>ManyPostResource::collection($this->posts()),
                        'tags'=>TagResource::collection($this->tags()),
                        'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                    ];
                }else{
                    return  [
                        'id'=>$this->id,
                        'name'=>$this->name,
                        'image'=>$this->image,
                        'descritpion'=>$this->description,
                        'admin'=>new ManyUserResource($this->admin),
                        'number_of_members'=>$this->members->count(),
                        'tags'=>TagResource::collection($this->tags()),
                        'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                    ];
                }
        }else{
            return  [
                'id'=>$this->id,
                'name'=>$this->name,
                'image'=>$this->image,
                'descritpion'=>$this->description,
                'admin'=>new ManyUserResource($this->admin),
                'members'=>ManyUserResource::collection($this->members->pluck('user')),
                'number_of_members'=>$this->members->count(),
                'posts'=>ManyPostResource::collection($this->posts()),
                'tags'=>TagResource::collection($this->tags()),
                'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
            ];
        }
    }
    private function user_rating(Request $request)
    {
       $rating = Rating::where('type','club')->where('rated_id',$this->id)
            ->where('user_id',$request->user()->id)->first();
        if ($rating){
            return ['id'=>$rating->id, 'rating'=>$rating->rating];
        }
        return null;
    }
}
