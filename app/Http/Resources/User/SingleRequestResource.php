<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Book\ManyBookResource;
use App\Http\Resources\Club\ManyClubResource;
use App\Http\Resources\Request\ResponseResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $user = null;
        if ($this->sender_id == $request->user()->id){
            $user = $this->receiver;
        }else if ($this->receiver_id == $request->user()->id){
            $user = $this->sender;
        }
        if (!$user){
            return [];
        }
        // return [new ManyUserResource($user)];
        if($this->response){
            if ($this->type == 'report'){
                return [
                    'id'=>$this->id,
                    'user'=>new ManyUserResource($user),
                    'reported_type'=>$this->reported_type,
                    'reported'=>$this->reported(),
                    'response'=>new ResponseResource($this->response),
                    'is_read'=>$this->is_read,
                    'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                ];
            }elseif($this->type == 'club'){
                return [
                    'id'=>$this->id,
                    'club'=>new ManyClubResource($this->club),
                    'response'=>new ResponseResource($this->response),
                    'is_read'=>$this->is_read,
                    'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                ];
            }elseif($this->type == 'friend'){
                return [
                    'id'=>$this->id,
                    'user'=>new ManyUserResource($user),
                    'response'=>new ResponseResource($this->response),
                    'is_read'=>$this->is_read,
                    'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                ];
            }
            else{
                return [
                    'id'=>$this->id,
                    'user'=>new ManyUserResource($user),
                    'book'=>new ManyBookResource($this->book),
                    'is_read'=>$this->is_read,
                    'response'=>new ResponseResource($this->response),
                    'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                ];
            }
        }else{
            if ($this->type == 'report'){
                return [
                    'id'=>$this->id,
                    'user'=>new ManyUserResource($user),
                    'reported_type'=>$this->reported_type,
                    'reported'=>$this->reported(),
                    'is_read'=>$this->is_read,
                    'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                ];
            }elseif($this->type == 'club'){
                return [
                    'id'=>$this->id,
                    'club'=>new ManyClubResource($this->club),
                    'is_read'=>$this->is_read,
                    'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                ];
            }elseif($this->type == 'friend'){
                return [
                    'id'=>$this->id,
                    'user'=>new ManyUserResource($user),
                    'is_read'=>$this->is_read,
                    'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                ];
            }
            else{
                return [
                    'id'=>$this->id,
                    'user'=>new ManyUserResource($user),
                    'book'=>new ManyBookResource($this->book),
                    'is_read'=>$this->is_read,
                    'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                ];
            }
        }

    }
}
