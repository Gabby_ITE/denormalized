<?php

namespace App\Http\Resources\User;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DiscussionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        if ($this->discussion_id > 0){
            return [
                'id'=>$this->id,
                'discussion'=>[
                    'id'=>$this->discussion->id,
                    'user'=>new ManyUserResource($this->discussion->user),
                    'text'=>$this->discussion->text,
                    'created_at'=>Carbon::parse($this->discussion->created_at)->diffForHumans(),
                ],
                'user'=>new ManyUserResource($this->user),
                'text'=>$this->text,
                'reacts'=>$this->reacts(),
                'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
            ];
        }else{
            return [
                'id'=>$this->id,
                'user'=>new ManyUserResource($this->user),
                'text'=>$this->text,
                'reacts'=>$this->reacts(),
                'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
            ];
        }
    }
}
