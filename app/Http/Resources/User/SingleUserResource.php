<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class SingleUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        if ($this->closed == true){
            if ($this->friends() == 1 || $request->user()->role == 'admin'){
                return [
                    'id'=>$this->id,
                    'first_name'=>$this->first_name,
                    'last_name'=>$this->last_name,
                    'email'=>$this->email,
                    'image'=>$this->image,
                    'created_at'=>$this->created_at,
                    'updated_at'=>$this->updated_at,
                ];
            }
            return [
                'id'=>$this->id,
                'first_name'=>$this->first_name,
                'last_name'=>$this->last_name,
                'image'=>$this->image
            ];
        }
        return [
            'id'=>$this->id,
            'first_name'=>$this->first_name,
            'last_name'=>$this->last_name,
            'email'=>$this->email,
            'image'=>$this->image,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at,
        ];

    }
}
