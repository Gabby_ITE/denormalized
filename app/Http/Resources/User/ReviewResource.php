<?php

namespace App\Http\Resources\User;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'user'=>new ManyUserResource($this->user),
            'rating'=>$this->rating(),
            'text'=>$this->text,
            'is_burn'=>$this->is_burn,
            'reacts'=>$this->reacts(),
            'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
        ];
    }
}
