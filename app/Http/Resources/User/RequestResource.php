<?php

namespace App\Http\Resources\User;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $user = null;
        if ($this->sender_id == $request->user()->id){
            $user = $this->receiver;
        }else if ($this->receiver_id == $request->user()->id){
            $user = $this->sender;
        }
        if ($this->type == 'club'){
            return [
                'id'=>$this->id,
                'user'=> new ManyUserResource($user),
                'type'=>'club',
                'is_read'=>$this->is_read,
                'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
            ];
        }elseif ($this->type == 'book'){
            return [
                'id'=>$this->id,
                'user'=> new ManyUserResource($user),
                'type'=>'book',
                'is_read'=>$this->is_read,
                'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
            ];
        }elseif($this->type == 'report'){
            return [
                'id'=>$this->id,
                'user'=> new ManyUserResource($user),
                'type'=>'report',
                'is_read'=>$this->is_read,
                'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
            ];
        }else{
            return [
                'id'=>$this->id,
                'user'=> new ManyUserResource($user),
                'type'=>'friend',
                'is_read'=>$this->is_read,
                'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
            ];
        }
    }
}
