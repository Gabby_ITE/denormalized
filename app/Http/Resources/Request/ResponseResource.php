<?php

namespace App\Http\Resources\Request;

use App\Http\Resources\User\ManyUserResource;
use App\Http\Resources\User\RequestResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ResponseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $user = null;
        if ($this->sender_id == $request->user()->id){
            $user = $this->receiver;
        }else if ($this->receiver_id == $request->user()->id){
            $user = $this->sender;
        }
        return [
            'id'=>$this->id,
            'user'=>new ManyUserResource($user),
            'request_type'=>$this->type,
            'request'=>new RequestResource($this->request),
            'is_read'=>$this->is_read,
            'type'=>$this->type,
            'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
        ];
    }
}
