<?php

namespace App\Http\Resources\Book;

use App\Http\Resources\Author\ManyAuthorResource;
use App\Http\Resources\Combine\DescriptionResource;
use App\Http\Resources\TagResource;
use App\Models\User\Rating;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleBookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'cover'=>$this->cover,
            'file'=>$this->file,
            'release_date'=>$this->release_date,
            'user_rating'=>$this->user_rating($request),
            'ratings'=>$this->ratings(),
            'full_ratings'=>$this->full_ratings(),
            'status'=>$this->status($request),
            'authors'=>ManyAuthorResource::collection($this->authors()),
            'tags'=>TagResource::collection($this->tags()),
            'descriptions'=>DescriptionResource::collection($this->descriptions()),
        ];
    }

    private function user_rating(Request $request)
    {
       $rating = Rating::where('type','book')->where('rated_id',$this->id)
            ->where('user_id',$request->user()->id)->first();
        if ($rating){
            return ['id'=>$rating->id, 'rating'=>$rating->rating];
        }
        return null;
    }
    private function status(Request $request)
    {
        $status = $this->statuses()
                        ->where('user_id',$request->user()->id);
        if ($status->count())
            return [
                'id' =>$status[0]->id,
                'status' =>$status[0]->status,
                'progress'=>$status[0]->progress,
            ];
        return null;
    }
}
