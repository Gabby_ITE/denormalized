<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Author\StoreAuthorRequest;
use App\Http\Requests\Author\UpdateAuthorRequest;
use App\Http\Resources\Author\ManyAuthorResource;
use App\Http\Resources\Author\SingleAuthorResource;
use App\Models\Author;
use App\Models\User;
use App\Models\User\Rating;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function index()
    {
        if (auth()->user()->role == 'admin'){
            return ManyAuthorResource::collection(Author::all());
        }
        $authors = collect();
        $allAuthors = Author::all();
        foreach($allAuthors as $author){
            foreach(auth()->user()->authors as $authorId){
                if ($author->id == $authorId){
                    $authors->add($author);
                }
            }
        }
        return ManyAuthorResource::collection($authors->merge($allAuthors));
    }

    public function store(StoreAuthorRequest $request)
    {
        $author = Author::create([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'books'=>collect(),
            'series'=>collect(),
        ]);
        if ($request->image){
            $image = time() . '-' . $author->first_name . '.' . $request->file('image')->extension();
            $request->image->move(public_path('authors_images'),$image);
            $author->image = 'authors_images/'.$image;
            $author->save();
        }
        return new SingleAuthorResource($author);
    }

    public function show(Author $author)
    {
        return new SingleAuthorResource($author);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAuthorRequest $request, Author $author)
    {
        $author->update($request->validated());
        return new SingleAuthorResource($author);
    }

    public function updateImage(Request $request, Author $author)
    {
        $request->validate([
            'image'=>'file|image'
        ]);
        if (File::exists($author->image)){
            File::delete(public_path($author->image));
        }
        $image = time() . '-' . $author->first_name . '.' . $request->file('image')->extension();
        $request->image->move(public_path('authors_images'),$image);
        $author->image = 'authors_images/'.$image;
        $author->save();
        return new SingleAuthorResource($author);

    }

    public function destroy(Author $author)
    {
        if (File::exists($author->image)){
            File::delete(public_path($author->image));
        }

        $ratings = Rating::where('type','author')->where('rated_id',$author->id)->get();
        foreach($ratings as $rate){
            $rate->delete();
        }
        $books = $author->books();
        foreach($books as $book){
            $book->authors->delete($author->id);
        }
        $series = $author->series();
        foreach($series as $ser){
            $ser->authors->delete($author->id);
        }
        $users = User::all();
        foreach($users as $user){
            $user->favorite_authors->delete($author->id);
        }

        $author->delete();
    }

}
