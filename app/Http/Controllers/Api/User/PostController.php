<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\ManyPostResource;
use App\Http\Resources\User\SinglePostResource;
use App\Models\Club\Club;
use App\Models\User\Post;
use App\Models\User\React;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    public function index($posted)
    {
        if (str_contains(request()->fullUrl(),'user')){
            return ManyPostResource::collection(Post::where('type','user')
                ->where('user_id',$posted)->get());
        }else{
            return ManyPostResource::collection(Post::where('type','club')
                ->where('club_id',$posted)->get());
        }
    }
    public function show($posted,$post)
    {
        if (str_contains(request()->fullUrl(),'user')){
            return new SinglePostResource(
                Post::where('type','user')
                ->where('user_id',$posted)->where('id',$post)->first()
            );
        }else{
            return new SinglePostResource(
                Post::where('type','club')
                ->where('club_id',$posted)->where('id',$post)->first()
            );
        }
    }
    public function store(Request $request, $posted)
    {
        $request->validate([
            'text'=>'required|string',
        ]);
        $post_id = null;
        if ($request->post_id){
            $request->validate(['post_id'=>'exists:posts,id']);
            $post_id = $request->post_id;
        }

        if (str_contains(request()->fullUrl(),'user')){
            if ($posted != auth()->user()->id){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $post = Post::create([
                'post_id'=>$post_id,
                'user_id'=>$request->user()->id,
                'type'=>'user',
                'text'=>$request ->text,
            ]);
            if ($request->images){
                $imgs = collect();
                $counter = 0;
                foreach($request->images as $image){
                    $img = $counter++ . time() . '-' . $request->user()->first_name . '.' . $image->extension();
                    $image->move(public_path('users/posts/'),$img);
                    $img = 'users/posts/'.$img;
                    $imgs->add($img);
                }
                $post->images = $imgs->toArray();
                $post->save();
            }
            return new SinglePostResource ($post);
        }else{
            $club = Club::find($posted);
            if (!$club){
                return response()->json(['message'=>'you can\'t do that']);
            }
            $post = Post::create([
                'post_id'=>$post_id,
                'user_id'=>$request->user()->id,
                'club_id'=>$club->id,
                'type'=>'club',
                'text'=>$request->text,
            ]);
            if ($request->images){
                $imgs = collect();
                $counter = 0;
                foreach($request->images as $image){
                    $img =  $counter++ . time() . '-' . $request->user()->first_name . '.' . $image->extension();
                    $image->move(public_path('clubs/posts/'),$img);
                    $img = 'clubs/posts/'.$img;
                    $imgs->add($img);
                }
                $post->images = $imgs->toArray();
                $post->save();
            }
            return new SinglePostResource ($post);
        }
    }

    public function update(Request $request,$posted,$post)
    {
        $post = Post::find($post);
        if ($post->user_id != $request->user()->id){
            return response()->json(['message'=>'your not allowed to do that'],403);
        }
        $request->validate([
            'text'=>'required|string',
        ]);
        $post->update([
            'text'=>$request->text
        ]);
        return new SinglePostResource($post);
    }

    public function addImages(Request $request, $posted ,$post)
    {
        $post = Post::find($post);
        if ($post->user_id != $request->user()->id){
            return response()->json(['message'=>'your not allowed to do that'],403);
        }
        if (str_contains(request()->fullUrl(),'user')){
            if ($request->images){
                $imgs = collect();
                $counter = 0;
                foreach($request->images as $image){
                    $img = $counter++ . time() . '-' . $request->user()->first_name . '.' . $image->extension();
                    $image->move(public_path('users/posts/'),$img);
                    $img = 'users/posts/'.$img;
                    $imgs->add($img);
                }
                $post->images = collect($post->images)->merge($imgs)->toArray();
                $post->save();
            }
            return new SinglePostResource ($post);
        }else{
            $club = Club::find($posted);
            if (!$club){
                return response()->json(['message'=>'you can\'t do that']);
            }
            if ($request->images){
                $imgs = collect();
                $counter = 0;
                foreach($request->images as $image){
                    $img = $counter++ . time() . '-' . $request->user()->first_name . '.' . $image->extension();
                    $image->move(public_path('clubs/posts/'),$img);
                    $img = 'clubs/posts/'.$img;
                    $imgs->add($img);
                }
                $post->images = collect($post->images)->merge($imgs)->toArray();
                $post->save();
            }
            return new SinglePostResource ($post);
        }
    }

    public function deleteImages(Request $request, $posted, $post)
    {
        $post = Post::find($post);
        if ($post->user_id != $request->user()->id){
            return response()->json(['message'=>'your not allowed to do that'],403);
        }
        foreach($request->images as $image){
            if(File::exists($image)){
                File::delete(public_path($image));
            }
            // return $post->images
            $imgs = $post->images;
            $key = array_search($image,$imgs);
            if ($key>=0){
                unset($imgs[$key]);
                $post->images = $imgs;
                $post->save();
            }
        }
        return new SinglePostResource($post);
    }


    public function destroy(Request $request,$posted,$post)
    {
        $post = Post::find($post);
        if ($post->user_id != $request->user()->id){
            return response()->json(['message'=>'your not allowed to do that'],403);
        }
        $reacts = null;
        if (str_contains(request()->fullUrl(),'user')){
            $reacts = React::where('type','user_post')->where('reacted_id',$post)->get();
        }else{
            $reacts = React::where('type','club_post')->where('reacted_id',$post)->get();
        }
        foreach($reacts as $react){
            $react->delete();
        }
        foreach($post->images as $image){
            if(File::exists($image)){
                File::delete(public_path($image));
            }
        }
        $post->delete();
        return response()->noContent();
    }
}
