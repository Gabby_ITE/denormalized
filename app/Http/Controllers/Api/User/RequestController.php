<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\SingleRequestResource;
use App\Http\Resources\User\RequestResource;
use App\Models\Club\Club;
use App\Models\User\Request as UserRequest;
use Illuminate\Http\Request;

class RequestController extends Controller
{

    public function index()
    {
        $send = auth()->user()->send_requests;
        $received = auth()->user()->received_requests;
        return RequestResource::collection($send->merge($received)->sortBy('is_read')->sortByDesc('created_at'));
    }

    public function show(UserRequest $request)
    {
        $request->update(['is_read'=>1]);
        return new SingleRequestResource($request);
    }
    public function reports()
    {
        return RequestResource::collection(auth()->user()->reports()->get()->sortBy('is_read')->sortByDesc('created_at'));
    }

    public function book_requests()
    {
        if (auth()->user()->role == 'admin'){
            return RequestResource::collection(auth()->user()->received_book_requests()->get()->sortBy('is_read')->sortByDesc('created_at'));
        }
        return RequestResource::collection(auth()->user()->book_requests()->get()->sortBy('is_read')->sortByDesc('created_at'));
    }

    public function club_requests()
    {
        $send = auth()->user()->send_club_requests()->get();
        $received = auth()->user()->receive_club_requests()->get();
        return RequestResource::collection($send->merge($received)->sortBy('is_read')->sortByDesc('created_at'));
    }

    public function friend_requests()
    {
        $send = auth()->user()->send_friend_requests()->get();
        $received = auth()->user()->receive_friend_requests()->get();
        return RequestResource::collection($send->merge($received)->sortBy('is_read')->sortByDesc('created_at'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'type'=>'required|in:club,friend,report,book'
        ]);
        if ($request->type=='friend'){
            $request->validate([
                'receiver_id'=>['required','exists:users,id']
            ]);

            if ($this->checkFriendExists($request)){
                return response()->json(['message'=>'you can\'t do that ']);
            }

            $friendRequest = UserRequest::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$request->receiver_id,
                'type'=>'friend',
            ]);
            return new SingleRequestResource($friendRequest);
        }elseif($request->type=='club'){
            $request->validate([
                'club_id'=>['required','exists:clubs,id']
            ]);
            $club = Club::find($request->club_id);
            if ($this->checkClubExists($request,$club)){
                return response()->json(['message'=>'you can\'t do that']);
            }
            $clubRequest = UserRequest::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$club->admin_id,
                'club_id'=>$club->id,
                'type'=>'club',
            ]);
            return new SingleRequestResource($clubRequest);
        }elseif($request->type=='book'){
            $request->validate([
                'book_id'=>'required|exists:books,id'
            ]);

            $bookRequest = UserRequest::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>1,
                'book_id'=>$request->book_id,
                'type'=>'book',
            ]);
            return new SingleRequestResource($bookRequest);
        }else{
            // 'reported_type'=>[
                //     'required',
                //     'in:series_review,
                //     book_review,
                //     book_discussion,
                //     series_discussion,
                //     club_post,
                //     user_post'

                // ],
            $request->validate([
                'reported_type'=>'required|in:series_review,book_review,book_discussion,series_discussion,club_post,user_post'
            ]);
            if ($request->reported_type == 'series_review'){
                $request->validate(['reported_id'=>['required','exists:reviews,id']]);
            }
            if ($request->reported_type == 'book_review'){
                $request->validate(['reported_id'=>['required','exists:reviews,id']]);
            }
            if ($request->reported_type == 'book_discussion'){
                $request->validate(['reported_id'=>['required','exists:discussions,id']]);
            }

            if ($request->reported_type == 'series_discussion'){
                $request->validate(['reported_id'=>['required','exists:discussions,id']]);
            }
            if ($request->reported_type == 'club_post'){
                $request->validate(['reported_id'=>['required','exists:posts,id']]);
            }
            if ($request->reported_type == 'user_post'){
                $request->validate(['reported_id'=>['required','exists:posts,id']]);
            }

            if ($this->checkReportExists($request)){
                return response()->json(['message'=>'you can\'t do that'],403);
            }
            $report = UserRequest::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>1,
                'type'=>'report',
                'reported_type'=>$request->reported_type,
                'reported_id'=>$request->reported_id,
            ]);

            return new SingleRequestResource($report);
        }
    }
    private function checkClubExists(Request $request,Club $club)
    {
        if ($club->members()->where('user_id',$request->user()->id)->exists() == 1){
            return true;
        }
        return $request->user()->send_club_requests()->where('receiver_id',$club->admin_id)
                ->exists() == 1;
    }
    private function checkFriendExists(Request $request)
    {
        return $request->user()->send_friend_requests()->where('receiver_id',$request->receiver_id)
                ->exists() == 1
                || $request->user()->receive_friend_requests()->where('sender_id',$request->receiver_id)
                ->exists() == 1;
    }
    private function checkReportExists(Request $request)
    {
        return $request->user()->reports()->where('receiver_id',1)
                ->where('reported_type',$request->reported_type)
                ->where('reported_id',$request->reported_id)->exists() == 1;
    }

    public function destroy(UserRequest $userRequest)
    {
        if ($userRequest->response){
            return response()->json(['message'=>'wait until 13 friday'],403);
        }
        if ($userRequest->type == 'report'){
            return response()->json(['message'=>'you can\'t delete report'],403);
        }
        $userRequest->delete();
        return response()->noContent();
    }
}
