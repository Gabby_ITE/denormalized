<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\User\ValidationCodeRequest;
use App\Http\Resources\User\ManyUserResource;
use App\Http\Resources\User\SingleUserResource;
use App\Mail\VerifyEmail;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rules\Password as RulesPassword;

class UserController extends Controller
{
    public function index()
    {
        return ManyUserResource::collection(User::all());
    }

    public function show(User $user)
    {
        return new SingleUserResource($user);
    }

    public function update(UpdateUserRequest $request)
    {
        auth()->user()->updated($request->validated());
        if($request->image != null){
            $imagePath = auth()->user()->image;
            if (File::exists(auth()->user()->image)){
                File::delete(public_path($imagePath));
            }
            $image = time() . '-' . $request->name . '.' . $request->image->extension();
            $request->image->move(public_path('users/cover_images'),$image);
            auth()->user()->image = 'users/cover_images/'.$image;
            auth()->user()->save();
        }


        if (auth()->user()->role == 'admin'){
            if ($request->role == 'user' || $request->role == 'admin'){
                auth()->user()->update([
                    'role'=>$request->role
                ]);
            }
        }
        return new SingleUserResource(auth()->user());
    }

    public function updateImage(Request $request){
        $request->validate(['image'=>'file|image']);
        if (File::exists(auth()->user()->image)){
            File::delete(public_path(auth()->user()->image));
        }
        $image = time() . '-' . auth()->user()->name . '.' . $request->file('image')->extension();
        $request->image->move(public_path('users/cover_images'),$image);
        auth()->user()->update([
            'image'=>'users/cover_images/'.$image,
        ]);
        return new SingleUserResource(auth()->user());
    }

    public function destroy(User $user)
    {
        if (auth()->user() != $user ){
            $this->middleware(['admin']);
        }

        $imagePath = $user->image;
        File::delete(public_path($imagePath));

        $user->delete();
        return response()->noContent();
    }

    public function register(RegisterRequest $request)
    {
        $request->validated();
        $validationCode = rand(100000,999999);
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'validation_code' => $validationCode,
        ]);
        if ($request->role){
            $user->role = $request->role;
        }
        if ($request->image){
            $image = time() . '-' . $request->name . '.' . $request->image->extension();
            $request->image->move(public_path('users/cover_images'),$image);
            $user->image = 'users/cover_images/'.$image;
        }
        $user->save();
        try{
            Mail::to($user->email)->send(new VerifyEmail($user));

        }catch(Exception $excep){
            return response()->json([$excep->getMessage()],401) ;
        }
        return response()->json(['message' => 'User registered successfully.'],201);

    }
    public function validateRegistration(ValidationCodeRequest $request)
    {
        $request->validated();
        $user = User::where('email', $request->email)
        ->where('validation_code', $request->validation_code)
        ->first();

        if ($user) {
            $user->email_verified_at = now();
            $user->save();
            return response()->json([
                'message' => 'Registration complete.',
                'token' => $user->createToken('Api Token')->plainTextToken,
            ], 200);
        } else {
            // validation code does not match, show an error message
            return response()->json(['message' => 'Invalid validation code.'], 422);
        }
    }

    public function login(Request $request)
    {
        $request->validate([
            'email'=>['required','email'],
            'password'=>['required']
        ]);

        if (! auth()->attempt($request->only(['email','password']))){
            return response()->json([
                'message'=>'Credentials do not match'
            ],401);
        }
        $user = User::where('email',$request->email)->first();
        return response()->json([
            'data'=> [
                'user'=>$user,
                'token'=>$user->createToken('Api Token')->plainTextToken,
            ]
        ],200);
    }

    public function logout()
    {
        auth()->user()->currentAccessToken()->delete();
        return response()->noContent();
    }

    public function resetPassword()
    {
        $validationCode = rand(100000,999999);
        auth()->user()->update([
            'validation_code'=>$validationCode
        ]);
        Mail::to(auth()->user()->email)->send(new VerifyEmail(auth()->user()));
        return response()->json(['message' => ' "Validation Code" has been send'],201);
    }

    public function validateResetPassword(ValidationCodeRequest $request)
    {
        $request->validated();
        $request->validate([
            'password'=>['required','confirmed',RulesPassword::defaults()]
        ]);

        $user = User::where('email', $request->email)
        ->where('validation_code', $request->validation_code)
        ->first();

        if ($user) {
            $user->email_verified_at = now();
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json([
                'message' => 'you change your password.',
            ], 200);
        }
        else {
            return response()->json(['message' => 'Invalid validation code.'], 422);
        }
    }

}
