<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\ReviewResource;
use App\Models\User\React;
use App\Models\User\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function index($reviewed)
    {
        if (str_contains(request()->fullUrl(), 'book')){
            return ReviewResource::collection(Review::where('type','book')
                ->where('reviewed_id',$reviewed)->get());
        }else{
            return ReviewResource::collection(Review::where('type','series')
                ->where('reviewed_id',$reviewed)->get());
        }
    }
    public function store(Request $request,$reviewed)
    {
        $request->validate([
            'text'=>'required|string',
        ]);
        $review_id = null;
        if ($request->review_id){
            $replyed_review = Review::find($request->review_id);
            if ($replyed_review){
                $review_id = $request->review_id;
            }

        }
        if (str_contains(request()->fullUrl(), 'book')){
            $review = Review::create([
                'user_id'=>$request->user()->id,
                'review_id'=>$review_id,
                'reviewed_id'=>$reviewed,
                'type'=>'book',
                'text'=>$request->text,
            ]);
            return new ReviewResource($review);
        }else{
            $review = Review::create([
                'user_id'=>$request->user()->id,
                'review_id'=>$review_id,
                'reviewed_id'=>$reviewed,
                'type'=>'series',
                'text'=>$request->text,
            ]);
            return new ReviewResource($review);
        }
    }

    public function update(Request $request,$reviewed,$review)
    {
        $request->validate([
            'text'=>'required|string',
        ]);
        $review = Review::where('id',$review)->first();
        if ($review->user_id == $request->user()->id){
            $review->update([
                'text'=>$request->text
            ]);
            return new ReviewResource($review);
        }
        return response()->json(['you can\'t do that'],403);
    }

    public function destroy(Request $request,$reviewed,$review)
    {
        if (str_contains(request()->fullUrl(), 'book')){
            $reacts = React::where('type','book_review')->where('reacted_id',$review)->get();
        }else{
            $reacts = React::where('type','series_review')->where('reacted_id',$review)->get();
        }
        foreach($reacts as $react){
            $react->delete();
        }
        $review = Review::where('id',$review)->first();
        if ($review->user_id == $request->user()->id){
            $review->delete();
            return response()->noContent();
        }
        return response()->json(['you can\'t do that'],403);
    }
}
