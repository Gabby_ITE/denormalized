<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\DiscussionResource;
use App\Models\User\Discussion;
use App\Models\User\React;
use Illuminate\Http\Request;

class DiscussionController extends Controller
{
    public function index($discussed)
    {
        if (str_contains(request()->fullUrl(), 'book')){
            return DiscussionResource::collection(Discussion::where('type','book')
                ->where('discussed_id',$discussed)->get());
        }else{
            return DiscussionResource::collection(Discussion::where('type','series')
                ->where('discussed_id',$discussed)->get());
        }
    }
    public function store(Request $request,$discussed)
    {
        $request->validate([
            'text'=>'required|string',
        ]);
        $discussion_id = null;
        if ($request->discussion_id){
            $replyed_discussion = Discussion::find($request->discussion_id);
            if ($replyed_discussion){
                $discussion_id = $request->discussion_id;
            }

        }
        if (str_contains(request()->fullUrl(), 'book')){
            $discussion = Discussion::create([
                'user_id'=>$request->user()->id,
                'discussion_id'=>$discussion_id,
                'discussed_id'=>$discussed,
                'type'=>'book',
                'text'=>$request->text,
            ]);
            return new DiscussionResource($discussion);
        }else{
            $discussion = Discussion::create([
                'user_id'=>$request->user()->id,
                'discussion_id'=>$discussion_id,
                'discussed_id'=>$discussed,
                'type'=>'series',
                'text'=>$request->text,
            ]);
            return new DiscussionResource($discussion);
        }
    }

    public function update(Request $request,$discussed,$discussion)
    {
        $request->validate([
            'text'=>'required|string',
        ]);
        $discussion = Discussion::where('id',$discussion)->first();
        if ($discussion->user_id == $request->user()->id){
            $discussion->update([
                'text'=>$request->text
            ]);
            return new DiscussionResource($discussion);
        }
        return response()->json(['you can\'t do that'],403);
    }

    public function destroy(Request $request,$discussed,$discussion)
    {
        if (str_contains(request()->fullUrl(), 'book')){
            $reacts = React::where('type','book_discussion')->where('reacted_id',$discussion)->get();
        }else{
            $reacts = React::where('type','series_discussion')->where('reacted_id',$discussion)->get();
        }
        foreach($reacts as $react){
            $react->delete();
        }
        $discussion = Discussion::where('id',$discussion)->first();
        if ($discussion->user_id == $request->user()->id){
            $discussion->delete();
            return response()->noContent();
        }
        return response()->json(['you can\'t do that'],403);
    }
}
