<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\Author\SingleAuthorResource;
use App\Http\Resources\Book\SingleBookResource;
use App\Http\Resources\Club\SingleClubResource;
use App\Http\Resources\Series\SingleSeriesResource;
use App\Models\Author;
use App\Models\Book\Book;
use App\Models\Club\Club;
use App\Models\Series;
use App\Models\User\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    public function store(Request $request, $rated)
    {
        $request->validate([
            'rating'=>'required|min:1|max:5'
        ]);
        if (str_contains(request()->fullUrl(), 'series')){
            if (Rating::where('type','series')->where('rated_id',$rated)->first()){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $rated = Series::find($rated);
            Rating::create([
                'type'=>'series',
                'rated_id'=>$rated->id,
                'user_id'=>$request->user()->id,
                'rating'=>$request->rating,
            ]);
            return new SingleSeriesResource($rated);
        }elseif (str_contains(request()->fullUrl(), 'book')){
            if (Rating::where('type','book')->where('rated_id',$rated)->first()){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $rated = Book::find($rated);
            Rating::create([
                'type'=>'book',
                'rated_id'=>$rated->id,
                'user_id'=>$request->user()->id,
                'rating'=>$request->rating,
            ]);
            return new SingleBookResource($rated);
        }elseif (str_contains(request()->fullUrl(), 'club')){
            if (Rating::where('type','club')->where('rated_id',$rated)->first()){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $rated = Club::find($rated);
            if (!$rated->members->where('user_id',$request->user()->id)->first()){
                return response()->json(['message'=>'you not alowed'],403);
            }
            Rating::create([
                'type'=>'club',
                'rated_id'=>$rated->id,
                'user_id'=>$request->user()->id,
                'rating'=>$request->rating,
            ]);
            return new SingleClubResource($rated);
        }else{
            if (Rating::where('type','author')->where('rated_id',$rated)->first()){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $rated = Author::find($rated);
            Rating::create([
                'type'=>'author',
                'rated_id'=>$rated->id,
                'user_id'=>$request->user()->id,
                'rating'=>$request->rating,
            ]);
            return new SingleAuthorResource($rated);
        }
    }

    public function update(Request $request, $rated)
    {
        if (str_contains(request()->fullUrl(), 'series')){
            $rating = Rating::where('type','series')->where('rated_id',$rated)->first();
            $rating->update([
                'rating'=>$request->rating
            ]);
            $rated = Series::find($rated);
            return new SingleSeriesResource($rated);
        }elseif (str_contains(request()->fullUrl(), 'book')){
            $rating = Rating::where('type','book')->where('rated_id',$rated)->first();
            $rating->update([
                'rating'=>$request->rating
            ]);
            $rated = Book::find($rated);
            return new SingleBookResource($rated);
        }elseif (str_contains(request()->fullUrl(), 'club')){
            $rated = Club::find($rated);
            if (!$rated->members->where('user_id',$request->user()->id)->first()){
                return response()->json(['message'=>'you not alowed'],403);
            }
            $rating = Rating::where('type','club')->where('rated_id',$rated->id)->first();
            $rating->update([
                'rating'=>$request->rating
            ]);
            return new SingleClubResource($rated);
        }else{
            $rating = Rating::where('type','author')->where('rated_id',$rated)->first();
            $rating->update([
                'rating'=>$request->rating
            ]);
            $rated = Author::find($rated);
            return new SingleAuthorResource($rated);
        }
    }

    public function destroy($rated)
    {
        if (str_contains(request()->fullUrl(), 'series')){
            $rating = Rating::where('type','series')->where('rated_id',$rated)->first();
            $rating->delete();
        }elseif (str_contains(request()->fullUrl(), 'book')){
            $rating = Rating::where('type','book')->where('rated_id',$rated)->first();
            $rating->delete();
        }elseif (str_contains(request()->fullUrl(), 'club')){
            $rating = Rating::where('type','club')->where('rated_id',$rated)->first();
            $rating->delete();
        }else{
            $rating = Rating::where('type','author')->where('rated_id',$rated)->first();
            $rating->delete();
        }
        return response()->noContent();
    }
}
