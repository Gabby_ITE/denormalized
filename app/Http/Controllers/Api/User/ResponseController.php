<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\Request\ResponseResource;
use App\Http\Resources\User\SingleRequestResource;
use App\Http\Resources\User\RequestResource;
use App\Models\Book\Book;
use App\Models\Club\ClubMember;
use App\Models\User\Friend;
use App\Models\User\Request as UserRequest;
use App\Models\User\Response;
use Illuminate\Http\Request;

class ResponseController extends Controller
{
    public function index()
    {
        $received = auth()->user()->received_responses;
        $send = auth()->user()->send_responses;
        return ResponseResource::collection(
            $send->merge($received)
        );
    }

    public function handle(Request $request, UserRequest $userRequest)
    {
        if ($userRequest->type == 'friend'){
            return $this->friend($request,$userRequest);
        }elseif($userRequest->type == 'club'){
            return $this->club($request,$userRequest);
        }elseif($userRequest->type == 'book'){
            return $this->book($request,$userRequest);
        }else{
            return $this->report($request,$userRequest);
        }
    }

    private function friend(Request $request,UserRequest $userRequest)
    {
        if ($userRequest == null){
            return response()->json(['message'=>'you can\'t do that null'],403);
        }
        if ($userRequest->response){
            return response()->json(['message'=>'you can\'t do that has a response'],403);
        }
        if ($request->user()->id != $userRequest->receiver_id){
            return response()->json(['message'=>'you are not the receiver'],403);
        }
        $request->validate(['type'=>['required','in:accept,reject']]);
        if ($request->type == 'accept'){

            if (Friend::where('sender_id',$userRequest->sender_id)
                    ->where('receiver_id',$userRequest->receiver_id)->exists()
                || Friend::where('receiver_id',$userRequest->sender_id)
                    ->where('sender_id',$userRequest->receiver_id)->exists()){
                    return response()->json(['message'=>' you can not do that'],403);
            }
            Friend::create([
                'sender_id'=>$userRequest->sender_id,
                'receiver_id'=>$userRequest->receiver_id,
            ]);
        }

        Response::create([
            'sender_id'=>$request->user()->id,
            'receiver_id'=>$userRequest->sender_id,
            'type'=>$request->type,
            'request_type'=>'friend',
            'request_id'=>$userRequest->id,
        ]);
        // $userRequest->update[['is_read'=>false]];
        return new SingleRequestResource($userRequest);
    }

    private function club(Request $request, UserRequest $userRequest)
    {
        if ($userRequest == null ){
            return response()->json(['message'=>'you can\'t do that'],403);
        }

        if ($userRequest->response){
            return response()->json(['message'=>'you can\'t do that has a response'],403);
        }
        if ($request->user()->id != $userRequest->receiver_id){
            return response()->json(['message'=>'you are not the receiver'],403);
        }
        $request->validate(['type'=>['required','in:accept,reject']]);
        if ($request->type == 'accept'){

            ClubMember::create([
                'user_id'=>$userRequest->sender_id,
                'club_id'=>$userRequest->club_id,
            ]);
            Response::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$userRequest->sender_id,
                'type'=>$request->type,
                'request_id'=>$userRequest->id,
            ]);
        }else{
            Response::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$userRequest->sender_id,
                'type'=>$request->type,
                'request_id'=>$userRequest->id,
            ]);
        }
        $userRequest->update(['is_read'=>false]);
        return new SingleRequestResource($userRequest);
    }

    private function book(Request $request, UserRequest $userRequest)
    {
        if ($userRequest == null || $userRequest->response){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        if ($request->user()->id != $userRequest->receiver_id){
            return response()->json(['message'=>'you are not the receiver'],403);
        }
        $request->validate(['type'=>['required','in:accept,reject']]);
        if ($request->type== 'accept'){
            $book = Book::find($userRequest->book_id);
            if ($book){
                $book->update(['is_private'=>false]);
                Response::create([
                    'sender_id'=>$request->user()->id,
                    'receiver_id'=>$userRequest->sender_id,
                    'type'=>$request->type,
                    'request_id'=>$userRequest->id,
                ]);
            }
        }else{
            Response::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$userRequest->sender_id,
                'type'=>$request->type,
                'request_id'=>$userRequest->id,
            ]);
        }
        $userRequest->update(['is_read'=>false]);
        return new SingleRequestResource($userRequest);
    }

    private function report(Request $request, UserRequest $userRequest)
    {
        if ($userRequest == null || $userRequest->response){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        $request->validate(['type'=>['required','in:accept,reject']]);
        if ($request->type == 'accept'){
            $userRequest->reported()->delete();
            Response::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$userRequest->sender_id,
                'type'=>$request->type,
                'request_id'=>$userRequest->id,
            ]);
        }else{
            Response::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$userRequest->sender_id,
                'type'=>$request->type,
                'request_id'=>$userRequest->id,
            ]);
        }
        $userRequest->update(['is_read'=>false]);
        return new SingleRequestResource($userRequest);
    }
}
