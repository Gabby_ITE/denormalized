<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\DiscussionResource;
use App\Http\Resources\User\ReviewResource;
use App\Http\Resources\User\SinglePostResource;
use App\Models\User\Discussion;
use App\Models\User\Post;
use App\Models\User\React;
use App\Models\User\Review;
use Illuminate\Http\Request;

class ReactController extends Controller
{
    public function store(Request $request,$page ,$reacted)
    {
        $request->validate([
            'react'=>'in:smile,angry,like,hot,love,cry|required'
        ]);
        if (str_contains(request()->fullUrl(),'series') && str_contains(request()->fullUrl(),'review') ){
            if (React::where('reacted_id',$reacted)->where('type','series_review')->where('user_id',$request->user()->id)->first()){
                return response()->json(['message'=>'you are not allowed'],403);
            }
            $reacted = Review::where('type','series')->where('id',$reacted)->first();
            React::create([
                'type'=>'series_review',
                'reacted_id'=>$reacted->id,
                'user_id'=>$request->user()->id,
                'react'=>$request->react,
            ]);
            return new ReviewResource($reacted);
        }elseif (str_contains(request()->fullUrl(), 'book') && str_contains(request()->fullUrl(), 'review') ){
            if (React::where('reacted_id',$reacted)->where('type','book_review')->where('user_id',$request->user()->id)->first()){
                return response()->json(['message'=>'you are not allowed'],403);
            }
            $reacted = Review::where('type','book')->where('id',$reacted)->first();
            React::create([
                'type'=>'book_review',
                'reacted_id'=>$reacted->id,
                'user_id'=>$request->user()->id,
                'react'=>$request->react,
            ]);
            return new ReviewResource($reacted);
        }elseif (str_contains(request()->fullUrl(), 'book') && str_contains(request()->fullUrl(), 'discussion') ){
            if (React::where('reacted_id',$reacted)->where('type','book_discussion')->where('user_id',$request->user()->id)->first()){
                return response()->json(['message'=>'you are not allowed'],403);
            }
            $reacted = Discussion::where('type','book')->where('id',$reacted)->first();
            React::create([
                'type'=>'book_discussion',
                'reacted_id'=>$reacted->id,
                'user_id'=>$request->user()->id,
                'react'=>$request->react,
            ]);
            return new DiscussionResource($reacted);
        }elseif (str_contains(request()->fullUrl(), 'series') && str_contains(request()->fullUrl(), 'discussion') ){
            if (React::where('reacted_id',$reacted)->where('type','series_discussion')->where('user_id',$request->user()->id)->first()){
                return response()->json(['message'=>'you are not allowed'],403);
            }
            $reacted = Discussion::where('type','series')->where('id',$reacted)->first();
            React::create([
                'type'=>'series_discussion',
                'reacted_id'=>$reacted->id,
                'user_id'=>$request->user()->id,
                'react'=>$request->react,
            ]);
            return new DiscussionResource($reacted);
        }elseif (str_contains(request()->fullUrl(), 'club') && str_contains(request()->fullUrl(), 'post') ){
            if (React::where('reacted_id',$reacted)->where('type','club_post')->where('user_id',$request->user()->id)->first()){
                return response()->json(['message'=>'you are not allowed'],403);
            }
            $reacted = Post::where('type','club')->where('id',$reacted)->first();
            React::create([
                'type'=>'club_post',
                'reacted_id'=>$reacted->id,
                'user_id'=>$request->user()->id,
                'react'=>$request->react,
            ]);
            return new SinglePostResource($reacted);
        }else{
            if (React::where('reacted_id',$reacted)->where('type','user_post')->where('user_id',$request->user()->id)->first()){
                return response()->json(['message'=>'you are not allowed'],403);
            }
            $reacted = Post::where('type','user')->where('id',$reacted)->first();
            React::create([
                'type'=>'user_post',
                'reacted_id'=>$reacted->id,
                'user_id'=>$request->user()->id,
                'react'=>$request->react,
            ]);
            return new SinglePostResource($reacted);
        }
    }

    public function update(Request $request,$page, $reacted)
    {
        $request->validate([
            'react'=>'in:smile,angry,like,hot,love,cry|required'
        ]);
        if (str_contains(request()->fullUrl(), 'series') && str_contains(request()->fullUrl(), 'review') ){
            $react = React::where('reacted_id',$reacted)->where('type','series_review')->where('user_id',$request->user()->id)->first();
            if ($react &&( $react->user_id != auth()->user()->id)){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $reacted = Review::where('type','series')->where('id',$reacted)->first();
            $react->update([
                'react'=>$request->react
            ]);
            return new ReviewResource($reacted);
        }elseif (str_contains(request()->fullUrl(), 'book') && str_contains(request()->fullUrl(), 'review') ){
            $react = React::where('reacted_id',$reacted)->where('type','book_review')->where('user_id',$request->user()->id)->first();
            if ($react &&( $react->user_id != auth()->user()->id)){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $reacted = Review::where('type','book')->where('id',$reacted)->first();
            $react->update([
                'react'=>$request->react
            ]);
            return new ReviewResource($reacted);
        }elseif (str_contains(request()->fullUrl(), 'book') && str_contains(request()->fullUrl(), 'discussion') ){
            $react = React::where('reacted_id',$reacted)->where('type','book_discussion')->where('user_id',$request->user()->id)->first();
            if ($react &&( $react->user_id != auth()->user()->id)){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $reacted = Discussion::where('type','book')->where('id',$reacted)->first();
            $react->update([
                'react'=>$request->react
            ]);
            return new DiscussionResource($reacted);
        }elseif (str_contains(request()->fullUrl(), 'series') && str_contains(request()->fullUrl(), 'discussion') ){
            $react = React::where('reacted_id',$reacted)->where('type','series_discussion')->where('user_id',$request->user()->id)->first();
            if ($react &&( $react->user_id != auth()->user()->id)){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $reacted = Discussion::where('type','series')->where('id',$reacted)->first();
            $react->update([
                'react'=>$request->react
            ]);
            return new DiscussionResource($reacted);
        }elseif (str_contains(request()->fullUrl(), 'club') && str_contains(request()->fullUrl(), 'post') ){
            $react = React::where('reacted_id',$reacted)->where('type','club_post')->where('user_id',$request->user()->id)->first();
            if ($react &&( $react->user_id != auth()->user()->id)){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $reacted = Post::where('type','club')->where('id',$reacted)->first();
            $react->update([
                'react'=>$request->react
            ]);
            return new SinglePostResource($reacted);
        }else{
            $react = React::where('reacted_id',$reacted)->where('type','user_post')->where('user_id',$request->user()->id)->first();
            if ($react && ($react->user_id != auth()->user()->id)){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $reacted = Post::where('type','user')->where('id',$reacted)->first();
            $react->update([
                'react'=>$request->react
            ]);
            return new SinglePostResource($reacted);
        }
    }

    public function destroy($page, $reacted)
    {

        if (str_contains(request()->fullUrl(), 'series') && str_contains(request()->fullUrl(), 'review') ){
            $react = React::where('reacted_id',$reacted)->where('type','series_review')->where('user_id',auth()->user()->id)->first();
            if ($react &&( $react->user_id != auth()->user()->id)){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $react->delete();
        }elseif (str_contains(request()->fullUrl(), 'book') && str_contains(request()->fullUrl(), 'review') ){
            $react = React::where('reacted_id',$reacted)->where('type','book_review')->where('user_id',auth()->user()->id)->first();
            if ($react &&( $react->user_id != auth()->user()->id)){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $react->delete();
        }elseif (str_contains(request()->fullUrl(), 'book') && str_contains(request()->fullUrl(), 'discussion') ){
            $react = React::where('reacted_id',$reacted)->where('type','book_discussion')->where('user_id',auth()->user()->id)->first();
            if ($react &&( $react->user_id != auth()->user()->id)){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $react->delete();
        }elseif (str_contains(request()->fullUrl(), 'series') && str_contains(request()->fullUrl(), 'discussion') ){
            $react = React::where('reacted_id',$reacted)->where('type','series_discussion')->where('user_id',auth()->user()->id)->first();
            if ($react &&( $react->user_id != auth()->user()->id)){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $react->delete();
        }elseif (str_contains(request()->fullUrl(), 'club') && str_contains(request()->fullUrl(), 'post') ){
            $react = React::where('reacted_id',$reacted)->where('type','club_post')->where('user_id',auth()->user()->id)->first();
            if ($react &&( $react->user_id != auth()->user()->id)){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $react->delete();
        }else{
            $react = React::where('reacted_id',$reacted)->where('type','user_post')->where('user_id',auth()->user()->id)->first();
            if ($react &&( $react->user_id != auth()->user()->id)){
                return response()->json(['message'=>'you not allowed'],403);
            }
            $react->delete();
        }
        return response()->noContent();
    }
}
