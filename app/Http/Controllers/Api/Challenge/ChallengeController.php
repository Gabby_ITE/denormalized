<?php

namespace App\Http\Controllers\Api\Challenge;

use App\Http\Controllers\Controller;
use App\Http\Requests\Challenge\StoreChallengeRequest;
use App\Http\Requests\Challenge\UpdateChallengeRequest;
use App\Http\Resources\Challenge\ChallengeProgressResource;
use App\Http\Resources\Challenge\ManyChallengeResource;
use App\Http\Resources\Challenge\SingleChallengeResource;
use App\Models\Book\Book;
use App\Models\Challenge\Challenge;
use App\Models\Challenge\ChallengeProgress;
use Illuminate\Http\Request;

class ChallengeController extends Controller
{
    public function index()
    {
        if(auth()->user()->role == 'admin'){
            return ManyChallengeResource::collection(Challenge::get()->sortByDesc('updated_at'));
        }
        return ManyChallengeResource::collection(
            Challenge::where('is_private',false)->get()->sortByDesc('updated_at')
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreChallengeRequest $request)
    {
        $request->validate([
            'book_id'=>['exists:books,id']
        ]);

        $request->validated();
        if ($request->start_date > $request->end_date){
            return response()->json([
                'message'=>'you can not do that'
            ]);
        }
        $is_private = 1;
        if ($request->is_private == 0 || $request->is_private == 1){
            $is_private = $request->is_private;
        }
        $book_id = null;
        $max_number = $request->max_number;
        if ($request->book_id){
            $book_id = $request->book_id;
            if ($request->type == 'pages'){
                $book = Book::find($book_id);
                if ($max_number>$book->number_of_pages){
                    $max_number = $book->number_of_pages;
                }
            }else{
                $max_number = 1;
            }
        }
        $challenge = Challenge::create([
            'creator_id'=>$request->user()->id,
            'book_id'=>$book_id,
            'name'=>$request->name,
            'is_private'=>$is_private,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'type'=>$request->type,
            'max_number'=>$max_number,
        ]);
        return new SingleChallengeResource($challenge);
    }

    /**
     * Display the specified resource.
     */
    public function show(Challenge $challenge)
    {
        if ($challenge->is_private == 1){
            if ($challenge->creator_id == auth()->user()->id || auth()->user()->role == 'admin')
                return new SingleChallengeResource($challenge);
            else{
                return response()->isInvalid();
            }
        }else{
            return new SingleChallengeResource($challenge);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateChallengeRequest $request, Challenge $challenge)
    {
        if ($challenge->creator_id != auth()->user()->id){
            return response()->json(['message'=>'not authorized'],403);
        }
        $challenge->update($request->validated());
        return new SingleChallengeResource($challenge);
    }

    public function destroy(Challenge $challenge)
    {
        if ($challenge->creator_id != auth()->user()->id || auth()->user()->role != 'admin'){
            return response()->json(['message'=>'not authorized'],403);
        }
        $challenge->deleteOrFail();
        return response()->noContent();
    }


    public function progresses()
    {
        return ChallengeProgressResource::collection(
            ChallengeProgress::where('user_id',auth()->user()->id)->get()->sortByDesc('created_at')
        );
    }
    public function show_progress(Challenge $challenge)
    {
        if ($challenge->progresses()->where('user_id',auth()->user()->id)->first()){
            return new ChallengeProgressResource(
                $challenge->progresses()->where('user_id',auth()->user()->id)->first()
            );
        }
    }
    public function take(Request $request,Challenge $challenge)
    {
        if ($challenge->progresses()->where('user_id',$request->user()->id)->first()){
            return response()->json([
                'message'=>'you aleady add this challenge'
            ],403);
        }
        $challengeProgress = ChallengeProgress::create([
            'challenge_id'=>$challenge->id,
            'user_id'=>$request->user()->id,
            'type'=>$challenge->type,
            'acheived_number'=>0
        ]);
        return new ChallengeProgressResource($challengeProgress);
    }
    public function remove(Challenge $challenge)
    {
        if (!$challenge->progresses()->where('user_id',auth()->user()->id)->first()){
            return response()->json([
                'message'=>'you do not have it'
            ],403);
        }
        $challenge->progresses()->where('user_id',auth()->user()->id)->first()->delete();
        return response()->noContent();
    }
}
