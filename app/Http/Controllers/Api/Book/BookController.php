<?php

namespace App\Http\Controllers\Api\Book;

use App\Http\Controllers\Controller;
use App\Http\Requests\Book\StoreBookRequest;
use App\Http\Resources\Book\ManyBookResource;
use App\Http\Resources\Book\SingleBookResource;
use App\Models\Author;
use App\Models\Book\Book;
use App\Models\Combine\Description;
use App\Models\Tag;
use Smalot\PdfParser\Parser;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index()
    {
        if (auth()->user()->role == 'admin'){
            return ManyBookResource::collection(Book::all()->sortByDesc('release_date'));
        }
        $books = collect();
        $public_books = Book::where('is_private',false)->get()->sortByDesc('release_date');
        foreach($public_books as $book){
            foreach(auth()->user()->tags as $tag){
                if ($book->hasTag($tag)){
                    $books->add($book);
                }
            }
            foreach(auth()->user()->authors as $author){
                if ($book->hasAuthor($author)){
                    $books->add($book);
                }
            }
        }

        return ManyBookResource::collection($books->unique('id')->merge($public_books));
    }

    public function privateBooks()
    {
        return ManyBookResource::collection(
            Book::where('is_private',true)->where('user_id',auth()->user()->id)
                ->get()->sortByDesc('release_date')
        );

    }

    public function store(StoreBookRequest $request)
    {
        $request->validated();
        $is_private = 0;
        if ($request->user()->role != 'admin'){
            $is_private = 1;
        }

        $image = time() . '-' . $request->title . '.' . $request->file('cover')->extension();
        $request->cover->move(public_path('books/cover_images'),$image);
        $image='books/cover_images/'.$image;

        $file = time() . '-' . $request->title . '.' . $request->file('file')->extension();
        $request->file->move(public_path('books/files'),$file);
        $file='books/files/'.$file;

        $parser = new Parser();
        $pdf = $parser->parseFile($file);

        $book = Book::create([
            'user_id'=>$request->user()->id,
            'title'=>$request->title,
            'cover'=>$image,
            'file'=>$file,
            'number_of_pages'=>count($pdf->getPages()),
            'release_date'=>$request->release_date,
            'is_private'=>$is_private,
            'tags'=>collect(),
            'authors'=>collect(),
        ]);

        if ($request->authors){
            foreach($request->authors as $authorId){
                $author = Author::find($authorId);
                if ($author){
                    $book->authors = collect($book->authors)->add((int)$authorId)->unique()->toArray();
                    $author->books = collect($author->books)->add($book->id)->unique()->toArray();
                    $book->save();
                    $author->save();
                }
            }
        }

        if ($request->tags){
            foreach($request->tags as $tagId){
                $tag = Tag::all()->find($tagId);
                if ($tag){
                    $book->tags = collect($book->tags)->add((int)$tagId)->unique()->toArray();
                    $tag->books = collect($tag->books)->add($book->id)->unique()->toArray();
                    $book->save();
                    $tag->save();
                }
            }
        }

        if ($request->descriptions){
            foreach($request->descriptions as $description){
                Description::create([
                    'described_id'=>$book->id,
                    'type'=>'book',
                    'name'=>$description['name'],
                    'text'=>$description['text'],
                ]);
            }
        }

        return new SingleBookResource($book);
    }

    public function show(Book $book)
    {
        return new SingleBookResource($book);
    }

    public function update(Request $request, Book $book)
    {
        if ($book->user_id != auth()->user()->id || auth()->user()->role != 'admin'){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        if (auth()->user()->role == 'admin'){
            if ($request->is_private){
                $book->update(['is_private'=>$request->is_private]);
            }
        }
        if ($request->title){
            $book->update([
                'title'=>$request->title
            ]);
        }
        if ($request->release_date){
            $book->update([
                'release_date'=>$request->release_date
            ]);
        }
        if($request->cover != null){
            if (File::exists($book->cover)){
                File::delete(public_path($book->cover));
            }
            $image = time() . '-' . $book->title . '.' . $request->file('cover')->extension();
            $request->cover->move(public_path('books/cover_images'),$image);
            $book->update([
                'cover'=>'books/cover_images/'.$image,
            ]);
        }
        if($request->file != null){

            if (File::exists($book->file)){
                File::delete(public_path($book->file));
            }
            $file = time() . '-' . $book->title . '.' . $request->file('file')->extension();
            $request->file->move(public_path('books/files'),$file);
            $book->update([
                'file'=>'books/files/'.$file,
            ]);
            $parser = new Parser();
            $pdf = $parser->parseFile($book->file);

            $book->update(['number_of_pages'=>count($pdf->getPages())]);
        }
        return new SingleBookResource($book);
    }


    public function addTags(Request $request, Book $book)
    {
        if ($book->user_id != auth()->user()->id || auth()->user()->role != 'admin'){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        if ($request->tag_id){
            $tag = Tag::all()->find($request->tag_id);
            if ($tag){
                $book->tags = collect($book->tags)->add($request->tag_id)->unique()->toArray();
                $tag->books = collect($tag->books)->add($book->id)->unique()->toArray();
                $book->save();
                $tag->save();
            }
        }
        if ($request->tags){
            foreach($request->tags as $tagId){
                $tag = Tag::all()->find($tagId);
                if ($tag){
                    $book->tags = collect($book->tags)->add((int)$tagId)->unique()->toArray();
                    $tag->books = collect($tag->books)->add($book->id)->unique()->toArray();
                    $book->save();
                    $tag->save();
                }
            }
        }
        return new SingleBookResource($book);
    }

    public function deleteTags(Request $request, Book $book)
    {
        if ($book->user_id != auth()->user()->id || auth()->user()->role != 'admin'){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        if ($request->tag_id){
            $tag = tag::all()->find($request->tag_id);
            if ($tag){
                $tags = $book->tags;
                $key = array_search($request->tag_id, $tags);
                if ($key>=0){
                    unset($tags[$key]);
                    $book->tags = $tags;
                    $book->save();
                }
                $books = $tag->books;
                $key = array_search($book->id, $books);
                if ($key>=0){
                    unset($books[$key]);
                    $tag->books = $books;
                    $tag->save();
                }
            }
        }
        if ($request->tags){
            foreach($request->tags as $tagId){
                $tag = tag::all()->find($tagId);
                if ($tag){
                    if ($tag){
                        $tags = $book->tags;
                        $key = array_search($tagId, $tags);
                        if ($key>=0){
                            unset($tags[$key]);
                            $book->tags = $tags;
                            $book->save();
                        }
                        $books = $tag->books;
                        $key = array_search($book->id, $books);
                        if ($key>=0){
                            unset($books[$key]);
                            $tag->books = $books;
                            $tag->save();
                        }
                    }
                }
            }
        }
        return new SingleBookResource($book);
    }

    public function addAuthors(Request $request, Book $book)
    {
        if ($book->user_id != auth()->user()->id || auth()->user()->role != 'admin'){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        if ($request->author_id){
            $author = Author::all()->find($request->author_id);
            if ($author){
                $book->authors = collect($book->authors)->add($request->author_id)->unique()->toArray();
                $author->books = collect($author->books)->add($book->id)->unique()->toArray();
                $book->save();
                $author->save();
            }
        }
        if ($request->authors){
            foreach($request->authors as $authorId){
                $author = Author::all()->find($authorId);
                if ($author){
                    $book->authors = collect($book->authors)->add($authorId)->unique()->toArray();
                    $author->books = collect($author->books)->add($book->id)->unique()->toArray();
                    $book->save();
                    $author->save();
                }
            }
        }
        return new SingleBookResource($book);
    }

    public function deleteAuthors(Request $request, Book $book)
    {
        if ($book->user_id != auth()->user()->id || auth()->user()->role != 'admin'){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        if ($request->author_id){
            $author = Author::all()->find($request->author_id);
            if ($author){
                if ($author){
                    $authors = $book->authors;
                    $key = array_search($request->author_id, $authors);
                    if ($key>=0){
                        unset($authors[$key]);
                        $book->authors = $authors;
                        $book->save();
                    }
                    $books = $author->books;
                    $key = array_search($book->id, $books);
                    if ($key>=0){
                        unset($books[$key]);
                        $author->books = $books;
                        $author->save();
                    }
                }
            }
        }
        if ($request->authors){
            foreach($request->authors as $authorId){
                $author = Author::all()->find($authorId);
                if ($author){
                    $authors = $book->authors;
                    $key = array_search($authorId, $authors);
                    if ($key>=0){
                        unset($authors[$key]);
                        $book->authors = $authors;
                        $book->save();
                    }
                    $books = $author->books;
                    $key = array_search($book->id, $books);
                    if ($key>=0){
                        unset($books[$key]);
                        $author->save();
                    }
                }
            }
        }
        return new SingleBookResource($book);
    }

    public function addDescriptions(Request $request, Book $book)
    {
        if ($book->user_id != auth()->user()->id || auth()->user()->role != 'admin'){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        if ($request->name && $request->text){
            Description::create([
                'described_id'=>$book->id,
                'type'=>'book',
                'name'=>$request->name,
                'text'=>$request->text,
            ]);
        }

        if ($request->descriptions){
            foreach($request->descriptions as $description){
                Description::create([
                    'described_id'=>$book->id,
                    'type'=>'book',
                    'name'=>$description['name'],
                    'text'=>$description['text'],
                ]);
            }
        }
        return new SingleBookResource($book);
    }

    public function deleteDescriptions(Request $request, Book $book)
    {
        if ($book->user_id != auth()->user()->id || auth()->user()->role != 'admin'){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        if ($request->description_id){
            $d = Description::where('id',$request->description_id)->where('described_id',$book->id)->first();
            if ($d){
                $d->delete();
            }
        }
        if ($request->descriptions){
            foreach($request->descriptions as $description){
                $d = Description::where('id',$description)->where('described_id',$book->id)->first();
                if ($d){
                    $d->delete();
                }
            }
        }
        return new SingleBookResource($book);
    }

    public function destroy(Book $book)
    {
        if ($book->user_id != auth()->user()->id || auth()->user()->role != 'admin'){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        $id = $book->id;
        $tags = $book->tags();
        $authors = $book->authors();
        foreach($tags as $tag){
            $books = $tag->books;
            $key = array_search($id, $books);
            if ($key>=0){
                unset($books[$key]);
                $tag->books = $books;
                $tag->save();
            }
        }
        foreach($authors as $author){
            $books = $author->books;
            $key = array_search($id, $books);
            if ($key>=0){
                unset($books[$key]);
                $author->books = $books;
                $author->save();
            }
        }
        foreach($book->descriptions() as $description){
            $description->delete();
        }
        foreach($book->discussions() as $discussions){
            $discussions->delete();
        }
        if (File::exists($book->cover)){
            File::delete(public_path($book->cover));
        }
        if (File::exists($book->file)){
            File::delete(public_path($book->file));
        }
        $book->delete();
        return response()->noContent();
    }

}
