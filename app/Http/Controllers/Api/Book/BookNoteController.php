<?php

namespace App\Http\Controllers\Api\Book;

use App\Http\Controllers\Controller;
use App\Http\Requests\Book\StoreBookNoteRequest;
use App\Models\Book\Book;
use App\Models\Book\BookNote;
use Illuminate\Http\Request;

class BookNoteController extends Controller
{
    public function index(Book $book)
    {
        return auth()->user()->notes->where('book_id',$book->id);
    }
    public function store(StoreBookNoteRequest $request, Book $book)
    {
        $request->validated();
        $bookNote = BookNote::create([
            'user_id'=>$request->user()->id,
            'book_id'=>$book->id,
            'note'=>$request->note,
            'color'=>$request->color,
            'page_number'=>$request->page_number,
        ]);
        return response()->json(['book_note'=>$bookNote]);
    }
    public function show(Book $book, string $bookNote)
    {
        $bookNote = BookNote::find($bookNote);
        if ($bookNote == null || $bookNote->book_id != $book->id
            || $bookNote->user_id != auth()->user()->id){
                return response()->json(['message'=>'you can\'t do that '],403);
            }
        return response()->json(['book_note'=>$bookNote]);
    }
    public function update(Request $request, Book $book, string $bookNote)
    {
        $bookNote = BookNote::find($bookNote);
        if ($bookNote == null || $bookNote->book_id != $book->id
            || $bookNote->user_id != auth()->user()->id){
                return response()->json(['message'=>'you can\'t do that '],403);
            }
        if ($request->page_number){
            $request->validate(['page_number'=>'integer']);
            $bookNote->update([
                'page_number'=>$request->page_number
            ]);
        }
        if ($request->note){
            $request->validate(['note'=>'string']);
            $bookNote->update([
                'note'=>$request->note
            ]);
        }
        if ($request->color){
            $request->validate(['color'=>'string']);
            $bookNote->update([
                'color'=>$request->color
            ]);
        }
        return response()->json(['book_note'=>$bookNote]);
    }
    public function destroy(Book $book, string $note)
    {
        $bookNote = BookNote::find($note);
        if ($bookNote == null || $bookNote->book_id != $book->id
            || $bookNote->user_id != auth()->user()->id){
                return response()->json(['message'=>'you can\'t do that '],403);
            }
        $bookNote->delete();
        return response()->noContent();
    }
}
