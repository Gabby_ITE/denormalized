<?php

namespace App\Http\Controllers\Api\Book;

use App\Http\Controllers\Controller;
use App\Models\Book\Book;
use App\Models\Book\Bookmark;
use Illuminate\Http\Request;

class BookmarkController extends Controller
{
    public function index(Book $book)
    {
        return auth()->user()->bookmarks->where('book_id',$book->id)->get();
    }
    public function store(Request $request, Book $book)
    {
        $request->validate(['page_number'=>['required','integer']]);
        $bookmark = Bookmark::create([
            'user_id'=>$request->user()->id,
            'book_id'=>$book->id,
            'page_number'=>$request->page_number
        ]);
        return response()->json(['data'=>$bookmark]);
    }
    public function destroy(Book $book,string $bookmark)
    {
        $bookmark = Bookmark::find($bookmark);
        if ($bookmark == null || $bookmark->book_id != $book->id
            || $bookmark->user_id != auth()->user()->id){
                return response()->json(['message'=>'you can\'t do that']);
        }
        $bookmark->delete();
        return response()->json(['deleted'],204);
    }
}
