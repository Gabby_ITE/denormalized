<?php

namespace App\Http\Controllers\Api\Book;

use App\Http\Controllers\Controller;
use App\Models\Book\Book;
use App\Models\Book\BookStatus;
use App\Models\Challenge\Challenge;
use App\Models\Challenge\ChallengeProgress;
use Exception;
use Illuminate\Http\Request;

class BookStatusController extends Controller
{
    public function index(Book $book)
    {
        $this->middleware(['admin']);
        return response()->json([
            'data'=>$book->statuses()->select('id','status','progress')->with('user')->get()
        ]);
    }

    public function statuses()
    {
        return BookStatus::where('user_id',auth()->user()->id)
        ->select('id','status','progress')->get();
    }

    public function store(Request $request,Book $book)
    {
        $request->validate([
            'status'=>'in:reading,watch_later,finished|required'
        ]);
        if($book->statuses()->where('user_id',$request->user()->id)->first()){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        $value = 0;
        if ($request->status == 'reading'){
            $request->validate([
                'progress'=>'required',
            ]);
            if ($request->progress > $book->number_of_pages || $request->progress < 0){
                return response()->json(['message'=>'progress out of scope']);
            }
            $value = $request->progress;
        }
        if ($request->status == 'finished'){
            $value = $book->number_of_pages;
        }

        $bookStatus =  BookStatus::create([
            'book_id'=>$book->id,
            'user_id'=>$request->user()->id,
            'status'=>$request->status,
            'progress'=>$value,
        ]);
        $acheived_number = $bookStatus->progress;
        try{
            if ($bookStatus->status == 'finished'){
                $this->updateChallengeProgress($request, 'book', 1,0);
            }
            $this->updateChallengeProgress($request, 'pages', $acheived_number,$book->id);
        }catch(Exception $e){
            echo $e->getMessage();
            return null;
        }
        return response()->json([
            'data'=>$bookStatus
        ]);


    }

    public function update(Request $request, Book $book)
    {
        if ($request->status == 'finished') {
            $acheived_number = $book->number_of_pages - $book->statuses()
                                            ->where('user_id', $request->user()->id)
                                            ->first()->progress;
            // echo $acheived_number;
            $book->statuses()->where('user_id', $request->user()->id)
                ->update([
                    'status' => $request->status,
                    'progress' => $book->number_of_pages,
                ]);
            try{
                $this->updateChallengeProgress($request, 'pages', $acheived_number,$book->id);
                $this->updateChallengeProgress($request, 'books', 1,0);
            }catch(Exception $e){
                echo $e->getMessage();
                return null;
            }

        }

        if ($request->progress != null && $request->status != 'finished') {
            if ($request->progress > $book->number_of_pages || $request->progress < 0) {
                return response()->json(['message' => 'progress out of scope']);
            } else {
                $acheived_number = $request->progress - $book->statuses()
                                                    ->where('user_id', $request->user()->id)
                                                    ->first()->progress;

                $book->statuses()->where('user_id', $request->user()->id)
                    ->update([
                        'progress' => $request->progress
                    ]);
                try{
                    $this->updateChallengeProgress($request, 'pages', $acheived_number,$book->id);
                }catch(Exception $e){
                    echo $e->getMessage();
                }

            }
        }

        $bookStatus = $book->statuses()
                            ->where('user_id', $request->user()->id)
                            ->get();

        return response()->json([
            'data' => $bookStatus
        ]);
    }

    private function updateChallengeProgress(Request $request, string $type, int $acheivedNumber,string $bookId)
    {
        $progresses = null;
        if ($bookId > 0){
            if (ChallengeProgress::all()->isNotEmpty()){
                $progresses = $request->user()->progressed_challenges()
                ->where('status', 'not_decide')
                ->withType($type)
                ->where('book_id', $bookId)
                ->get();
            }else{
                return;
            }

            foreach($progresses as $progress){
                if ($acheivedNumber == 0){
                    break;
                }
                $challenge = Challenge::find($progress->challenge_id);
                if ($progress->acheived_number + $acheivedNumber >= $challenge->max_number){

                    $progress->update([
                        'acheived_number' =>$challenge->max_number,
                    ]);
                    $acheivedNumber = $progress->acheived_number + $acheivedNumber - $challenge->max_number;
                }else{

                    $progress->update([
                        'acheived_number' =>$progress->acheived_number + $acheivedNumber
                    ]);
                    $acheivedNumber = 0;
                }
                $progress->updateStatus();
            }
        }

        if (ChallengeProgress::all()->isNotEmpty()){
            $progresses = $request->user()->progressed_challenges()
            ->where('status', 'not_decide')
            ->withType($type)
            ->whereNull('book_id')
            ->get();
        }else{
            return;
        }
        foreach($progresses as $progress){
            if ($acheivedNumber == 0){
                break;
            }
            $challenge = Challenge::find($progress->challenge_id);
            if ($progress->acheived_number + $acheivedNumber >= $challenge->max_number){
                $progress->update([
                    'acheived_number' =>$challenge->max_number,
                ]);
                $acheivedNumber = $progress->acheived_number + $acheivedNumber - $challenge->max_number;
            }else{
                $progress->update([
                    'acheived_number' =>$progress->acheived_number + $acheivedNumber
                ]);
                $acheivedNumber = 0;
            }

            $progress->updateStatus();
        }
    }

    public function destory(Book $book)
    {
        $bookStatus =$book->statuses()->where('user_id',auth()->user()->id)->delete();
        return response()->noContent();
    }
}
