<?php

namespace App\Http\Controllers\Api\Club;

use App\Http\Controllers\Controller;
use App\Http\Requests\Club\StoreClubRequest;
use App\Http\Resources\Club\ManyClubPostResource;
use App\Http\Resources\Club\ManyClubResource;
use App\Http\Resources\Club\SingleClubResource;
use App\Models\Club\Club;
use App\Models\Club\ClubMember;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ClubController extends Controller
{
    public function index()
    {
        if (auth()->user()->role == 'admin'){
            return ManyClubResource::collection(Club::all()->sortByDesc('created_at'));
        }
        $clubs= collect();
        $public_clubs= Club::where('is_private',false)->get()->sortByDesc('created_at');
        foreach($public_clubs as $club){
            foreach(auth()->user()->tags as $tag){
                if ($club->hasTag($tag)){
                    $clubs->add($club);
                }
            }
        }

        return ManyClubResource::collection($clubs->unique('id')->merge($public_clubs));
    }

    public function store(StoreClubRequest $request)
    {
        $request->validated();
        $image = time() . '-' . $request->title . '.' . $request->file('image')->extension();
        $request->image->move(public_path('clubs/cover_images'),$image);
        $image='clubs/cover_images/'.$image;
        $is_closed = $request->is_closed;
        if ($request->private){
            $is_closed = $request->is_private;
        }
        $club = Club::create([
            'image'=>$image,
            'description'=>$request->description,
            'is_private'=>$request->is_private,
            'admin_id'=>$request->user()->id,
            'name'=>$request->name,
            'is_closed'=>$is_closed,
            'tags'=>collect(),
        ]);

        ClubMember::create([
            'user_id'=>$request->user()->id,
            'club_id'=>$club->id
        ]);
        if ($request->tags){
            foreach($request->tags as $tagId){
                $tag = Tag::all()->find($tagId);
                if ($tag){
                    $club->tags = collect($club->tags)->add((int)$tagId)->unique()->toArray();
                    $tag->clubs = collect($tag->clubs)->add($club->id)->unique()->toArray();
                    $club->save();
                    $tag->save();
                }
            }
        }
        return new SingleClubResource($club);
    }

    public function update(Request $request, Club $club)
    {
        if ($club->admin_id != auth()->user()->id){
            return response()->json(['message'=>'you are not allowed'],403);
        }
        if ($request->name){
            $club->update([
                'name'=>$request->name
            ]);
        }

        if ($request->is_private){
            $club->update([
                'is_private'=>$request->is_private
            ]);
        }
        if ($request->name){
            $club->update([
                'name'=>$request->name
            ]);
        }
        if ($request->is_closed){
            if (!$club->is_private){
                $club->update([
                    'is_closed'=>$request->is_closed
                ]);
            }
        }
        if ($request->description){
            $club->update([
                'description'=>$request->description
            ]);
        }
        if ($request->image){
            if (File::exists($club->image)){
                File::delete(public_path($club->image));
            }
            $image = time() . '-' . $club->name . '.' . $request->file('image')->extension();
            $request->image->move(public_path('clubs/cover_images'),$image);
            $image='clubs/cover_images/'.$image;
            $club->update([
                'image'=>$image,
            ]);
        }

        return new SingleClubResource($club);
    }

    public function show(Club $club)
    {
        return new SingleClubResource($club);
    }

    public function addTags(Request $request, Club $club)
    {
        if ($club->admin_id != auth()->user()->id){
            return response()->json(['message'=>'you are not allowed'],403);
        }
        if ($request->tag_id){
            $tag = Tag::all()->find($request->tag_id);
            if ($tag){
                $club->tags = collect($club->tags)->add($request->tag_id)->unique()->toArray();
                $tag->clubs = collect($tag->clubs)->add($club->id)->unique()->toArray();
                $club->save();
                $tag->save();
            }
        }
        if ($request->tags){
            foreach($request->tags as $tagId){
                $tag = Tag::all()->find($tagId);
                if ($tag){
                    $club->tags = collect($club->tags)->add((int)$tagId)->unique()->toArray();
                    $tag->clubs = collect($tag->clubs)->add($club->id)->unique()->toArray();
                    $club->save();
                    $tag->save();
                }
            }
        }
        return new SingleClubResource($club);
    }
    public function deleteTags(Request $request, Club $club)
    {
        if ($club->admin_id != auth()->user()->id){
            return response()->json(['message'=>'you are not allowed'],403);
        }
        if ($request->tag_id){
            $tag = tag::all()->find($request->tag_id);
            if ($tag){
                $tags = $club->tags;
                $key = array_search($request->tag_id, $tags);
                if ($key>=0){
                    unset($tags[$key]);
                    $club->tags = $tags;
                    $club->save();
                }
                $clubs = $tag->clubs;
                $key = array_search($club->id, $clubs);
                if ($key>=0){
                    unset($clubs[$key]);
                    $tag->clubs = $clubs;
                    $tag->save();
                }
            }
        }
        if ($request->tags){
            foreach($request->tags as $tagId){
                $tag = tag::all()->find($tagId);
                if ($tag){
                    if ($tag){
                        $tags = $club->tags;
                        $key = array_search($tagId, $tags);
                        if ($key>=0){
                            unset($tags[$key]);
                            $club->tags = $tags;
                            $club->save();
                        }
                        $clubs = $tag->clubs;
                        $key = array_search($club->id, $clubs);
                        if ($key>=0){
                            unset($clubs[$key]);
                            $tag->clubs = $clubs;
                            $tag->save();
                        }
                    }
                }
            }
        }
        return new SingleClubResource($club);
    }

    public function destroy(Club $club)
    {
        $id = $club->id;
        $tags = $club->tags();
        foreach($tags as $tag){
            $clubs = $tag->clubs;
            $key = array_search($id, $clubs);
            if ($key>=0){
                unset($clubs[$key]);
                $tag->clubs = $clubs;
                $tag->save();
            }
        }
        if (File::exists($club->image)){
            File::delete(public_path($club->image));
        }
        $club->delete();
        return response()->noContent();
    }
}
