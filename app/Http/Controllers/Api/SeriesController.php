<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Series\StoreSeriesRequest;
use App\Http\Resources\Series\ManySeriesResource;
use App\Http\Resources\Series\SingleSeriesResource;
use App\Models\Author;
use App\Models\Book\Book;
use App\Models\Combine\Description;
use App\Models\Series;
use App\Models\Tag;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class SeriesController extends Controller
{
    public function index()
    {
        if (auth()->user()->role == 'admin'){
            return ManySeriesResource::collection(Series::all()->sortByDesc('release_date'));
        }
        $seriess = collect();
        $public_series = Series::all()->sortByDesc('release_date');
        foreach($public_series as $series){
            foreach(auth()->user()->tags as $tag){
                if ($series->hasTag($tag)){
                    $seriess->add($series);
                }
            }
            foreach(auth()->user()->authors as $author){
                if ($series->hasTag($author)){
                    $seriess->add($series);
                }
            }
        }

        return ManySeriesResource::collection($seriess->unique('id')->merge($public_series));
    }
    public function store(StoreSeriesRequest $request)
    {
        $request->validated();

        $image = time() . '-' . $request->title . '.' . $request->file('cover')->extension();
        $request->cover->move(public_path('series/cover_images'),$image);
        $image='series/cover_images/'.$image;


        $series = Series::create([
            'title'=>$request->title,
            'cover'=>$image,
            'release_date'=>$request->release_date,
            'tags'=>collect(),
            'authors'=>collect(),
        ]);

        if ($request->authors){
            foreach($request->authors as $authorId){
                $author = Author::find($authorId);
                if ($author){
                    $series->authors = collect($series->authors)->add((int)$authorId)->unique()->toArray();
                    $author->series = collect($author->series)->add($series->id)->unique()->toArray();
                    $series->save();
                    $author->save();
                }
            }
        }

        if ($request->tags){
            foreach($request->tags as $tagId){
                $tag = Tag::all()->find($tagId);
                if ($tag){
                    $series->tags = collect($series->tags)->add((int)$tagId)->unique()->toArray();
                    $tag->series = collect($tag->series)->add($series->id)->unique()->toArray();
                    $series->save();
                    $tag->save();
                }
            }
        }

        if ($request->descriptions){
            foreach($request->descriptions as $description){
                Description::create([
                    'described_id'=>$series->id,
                    'type'=>'series',
                    'name'=>$description['name'],
                    'text'=>$description['text'],
                ]);
            }
        }

        if ($request->books){
            foreach($request->books as $bookId){
                $book = Book::find($bookId);
                if($book){
                    $book->update(['series_id'=>$series->id]);
                }
            }
        }

        return new SingleSeriesResource($series);
    }
    public function show(Series $serie)
    {
        return new SingleSeriesResource($serie);
    }

    public function update(Request $request, Series $serie)
    {
        if ($request->title){
            $serie->update([
                'title'=>$request->title
            ]);
        }
        if ($request->release_date){
            $serie->update([
                'release_date'=>$request->release_date
            ]);
        }
        if($request->cover != null){
            if (File::exists($serie->cover)){
                File::delete(public_path($serie->cover));
            }
            $image = time() . '-' . $serie->title . '.' . $request->file('cover')->extension();
            $request->cover->move(public_path('series/cover_images'),$image);
            $serie->update([
                'cover'=>'series/cover_images/'.$image,
            ]);
        }
        return new SingleSeriesResource($serie);
    }
    public function addTags(Request $request, Series $serie)
    {
        if ($request->tag_id){
                $tag = Tag::all()->find($request->tag_id);
                if ($tag){
                    $serie->tags = collect($serie->tags)->add($request->tag_id)->unique()->toArray();
                    $tag->series = collect($tag->series)->add($serie->id)->unique()->toArray();
                    $serie->save();
                    $tag->save();
                }
        }
        if ($request->tags){
            foreach($request->tags as $tagId){
                $tag = Tag::all()->find($tagId);
                if ($tag){
                    $serie->tags = collect($serie->tags)->add((int)$tagId)->unique()->toArray();
                    $tag->series = collect($tag->series)->add($serie->id)->unique()->toArray();
                    $serie->save();
                    $tag->save();
                }
            }
        }
        return new SingleSeriesResource($serie);
    }

    public function deleteTags(Request $request, Series $serie)
    {
        if ($request->tag_id){
            $tag = tag::all()->find($request->tag_id);
            if ($tag){
                $tags = $serie->tags;
                $key = array_search($request->tag_id, $tags);
                if ($key>=0){
                    unset($tags[$key]);
                    $serie->tags = $tags;
                    $serie->save();
                }
                $series = $tag->series;
                $key = array_search($serie->id, $series);
                if ($key>=0){
                    unset($series[$key]);
                    $tag->series = $series;
                    $tag->save();
                }
            }
        }
        if ($request->tags){
            foreach($request->tags as $tagId){
                $tag = tag::all()->find($tagId);
                if ($tag){
                    if ($tag){
                        $tags = $serie->tags;
                        $key = array_search($tagId, $tags);
                        if ($key>=0){
                            unset($tags[$key]);
                            $serie->tags = $tags;
                            $serie->save();
                        }
                        $series = $tag->series;
                        $key = array_search($serie->id, $series);
                        if ($key>=0){
                            unset($series[$key]);
                            $tag->series = $series;
                            $tag->save();
                        }
                    }
                }
            }
        }
        return new SingleSeriesResource($serie);
    }

    public function addAuthors(Request $request, Series $serie)
    {        if ($request->author_id){
            $author = Author::all()->find($request->author_id);
            if ($author){
                $serie->authors = collect($serie->authors)->add($request->author_id)->unique()->toArray();
                $author->series = collect($author->series)->add($serie->id)->unique()->toArray();
                $serie->save();
                $author->save();
            }
        }
        if ($request->authors){
            foreach($request->authors as $authorId){
                $author = Author::all()->find($authorId);
                if ($author){
                    $serie->authors = collect($serie->authors)->add($authorId)->unique()->toArray();
                    $author->series = collect($author->series)->add($serie->id)->unique()->toArray();
                    $serie->save();
                    $author->save();
                }
            }
        }
        return new SingleSeriesResource($serie);
    }

    public function deleteAuthors(Request $request, Series $serie)
    {
        if ($request->author_id){
            $author = Author::all()->find($request->author_id);
            if ($author){
                if ($author){
                    $authors = $serie->authors;
                    $key = array_search($request->author_id, $authors);
                    if ($key>=0){
                        unset($authors[$key]);
                        $serie->authors = $authors;
                        $serie->save();
                    }
                    $series = $author->series;
                    $key = array_search($serie->id, $series);
                    if ($key>=0){
                        unset($series[$key]);
                        $author->series = $series;
                        $author->save();
                    }
                }
            }
        }
        if ($request->authors){
            foreach($request->authors as $authorId){
                $author = Author::all()->find($authorId);
                if ($author){
                    $authors = $serie->authors;
                    $key = array_search($authorId, $authors);
                    if ($key>=0){
                        unset($authors[$key]);
                        $serie->authors = $authors;
                        $serie->save();
                    }
                    $series = $author->series;
                    $key = array_search($serie->id, $series);
                    if ($key>=0){
                        unset($series[$key]);
                        $author->save();
                    }
                }
            }
        }
        return new SingleSeriesResource($serie);
    }

    public function addDescriptions(Request $request, Series $serie)
    {
        if ($request->name && $request->text){
            Description::create([
                'described_id'=>$serie->id,
                'type'=>'series',
                'name'=>$request->name,
                'text'=>$request->text,
            ]);
        }

        if ($request->descriptions){
            foreach($request->descriptions as $description){
                Description::create([
                    'described_id'=>$serie->id,
                    'type'=>'series',
                    'name'=>$description['name'],
                    'text'=>$description['text'],
                ]);
            }
        }
        return new SingleSeriesResource($serie);
    }

    public function deleteDescriptions(Request $request, Series $serie)
    {
        if ($request->description_id){
            $d = Description::where('id',$request->description_id)->where('described_id',$serie->id)->first();
            if ($d){
                $d->delete();
            }
        }
        if ($request->descriptions){
            foreach($request->descriptions as $description){
                $d = Description::where('id',$description)->where('described_id',$serie->id)->first();
                if ($d){
                    $d->delete();
                }
            }
        }
        return new SingleSeriesResource($serie);
    }

    public function addBooks(Request $request, Series $serie)
    {
        if ($request->book_id){
            $book = Book::find($request->book_id);
                if($book){
                    $book->update(['series_id'=>$serie->id]);
                }
        }
        if ($request->books){
            foreach($request->books as $bookId){
                $book = Book::find($bookId);
                if($book){
                    $book->update(['series_id'=>$serie->id]);
                }
            }
        }
        return new SingleSeriesResource($serie);
    }
    public function deleteBooks(Request $request, Series $serie)
    {
        if ($request->book_id){
            $book = Book::find($request->book_id);
            if($book && $book->series_id == $serie->id){
                $book->update(['series_id'=>null]);
            }
        }
        if ($request->books){
            foreach($request->books as $bookId){
                $book = Book::find($bookId);
                if($book && $book->series_id == $serie->id){
                    $book->update(['series_id'=>null]);
                }
            }
        }
        return new SingleSeriesResource($serie);
    }
    public function destroy(series $serie)
    {
        $id = $serie->id;
        $tags = $serie->tags();
        $authors = $serie->authors();
        foreach($tags as $tag){
            $series = $tag->series;
            $key = array_search($id, $series);
            if ($key>=0){
                unset($series[$key]);
                $tag->series = $series;
                $tag->save();
            }
        }
        foreach($authors as $author){
            $series = $author->series;
            $key = array_search($id, $series);
            if ($key>=0){
                unset($series[$key]);
                $author->series = $series;
                $author->save();
            }
        }
        foreach($serie->descriptions() as $description){
            $description->delete();
        }
        foreach($serie->discussions() as $discussions){
            $discussions->delete();
        }
        if (File::exists($serie->cover)){
            File::delete(public_path($serie->cover));
        }
        if (File::exists($serie->file)){
            File::delete(public_path($serie->file));
        }
        $serie->delete();
        return response()->noContent();
    }
}

