<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TagResource;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index()
    {
        return TagResource::collection(Tag::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>['required','unique:tags,name']
        ]);
        $tag = Tag::create([
            'name'=>$request->name,
            'books'=>collect(),
            'clubs'=>collect(),
            'series'=>collect(),
        ]);

        return new TagResource($tag);
    }

    /**
     * Display the specified resource.
     */
    public function show(Tag $tag)
    {
        return new TagResource($tag);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Tag $tag)
    {
        $request->validate([
            'name'=>['required','unique:tags,name']
        ]);
        $tag->update([
            'name'=>$request->name,
        ]);

        return new TagResource($tag);
    }


    public function destroy(Tag $tag)
    {
        $id = $tag->id;
        $books = $tag->books();
        foreach($books as $book){
            $tags = $book->tags;
            $key = array_search($id, $tags);
            if ($key>=0){
                unset($tags[$key]);
                $book->tags = $tags;
                $book->save();
            }
        }
        $series = $tag->series();
        foreach($series as $serie){
            $tags = $serie->tags;
            $key = array_search($id, $tags);
            if ($key>=0){
                unset($tags[$key]);
                $serie->tags = $tags;
                $serie->save();
            }
        }
        $clubs = $tag->clubs();
        foreach($clubs as $club){
            $tags = $club->tags;
            $key = array_search($id, $tags);
            if ($key>=0){
                unset($tags[$key]);
                $club->tags = $tags;
                $club->save();
            }
        }

        $users = User::all();
        foreach($users as $user){
            $tags = $user->favorite_tags;
            $key = array_search($id, $tags);
            if ($key>=0){
                unset($tags[$key]);
                $user->favorite_tags = $tags;
                $user->save();
            }
        }
        $tag->delete();
    }
}
