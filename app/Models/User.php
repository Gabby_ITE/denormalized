<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Models\Author;
use App\Models\Book\Bookmark;
use App\Models\Book\BookNote;
use App\Models\Book\BookStatus;
use App\Models\Challenge\Challenge;
use App\Models\Challenge\ChallengeProgress;
use App\Models\Club\ClubMember;
use App\Models\Tag;
use App\Models\User\Friend;
use App\Models\User\Request;
use App\Models\User\Response;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'image',
        'email',
        'password',
        'validation_code',
        'role',
        'is_closed',
        'favorite_tags',
        'favorite_authors',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'favorite_tags'=>'array',
        'favorite_authors'=>'array',
    ];

    public function tags()
    {
        $tags = collect();
        foreach($this->favorite_tags as $tagId){
            $tag = Tag::find($tagId);
            $tags->add($tag);
        }
        return $tags;
    }
    public function authors()
    {
        $authors = collect();
        foreach($this->favorite_authors as $authorId){
            $author = Author::find($authorId);
            $authors->add($author);
        }
        return $authors;
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function reviews()
    {
        return $this->hasMany(Rating::class);
    }
    public function received_requests()
    {
        return $this->hasMany(Request::class,'receiver_id');
    }
    public function send_requests()
    {
        return $this->hasMany(Request::class,'sender_id');
    }
    public function received_responses()
    {
        return $this->hasMany(Response::class,'receiver_id');
    }
    public function send_responses()
    {
        return $this->hasMany(Response::class,'sender_id');
    }
    public function reacts()
    {
        return $this->hasMany(React::class);
    }
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
    public function discussions()
    {
        return $this->hasMany(Discussion::class);
    }
    public function friends()
    {
        $receivers = Friend::where('sender_id',$this->id)->pluck('receiver');
        $senders =  Friend::where('receiver_id',$this->id)->pluck('sender');
        return $receivers->merge($senders);
    }
    public function joined_clubs()
    {
        return $this->hasMany(ClubMember::class);
    }
    public function books_status()
    {
        return $this->hasMany(BookStatus::class);
    }
    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class);
    }
    public function notes()
    {
        return $this->hasMany(BookNote::class);
    }

    public function send_friend_requests()
    {
        return Request::where('sender_id',$this->id)->where('type','friend');
    }
    public function receive_friend_requests()
    {
        return Request::where('receiver_id',$this->id)->where('type','friend');
    }

    public function send_club_requests()
    {
        return Request::where('sender_id',$this->id)->where('type','club');
    }
    public function receive_club_requests()
    {
        return Request::where('receiver_id',$this->id)->where('type','club');
    }

    public function book_requests()
    {
        return Request::where('sender_id',$this->id)->where('type','book');
    }

    public function received_book_requests()
    {
        if ($this->role == 'admin'){
            return Request::where('receiver_id',$this->id)->where('type','book');
        }
        return null;
    }

    public function reports()
    {
        if ($this->role == 'admin'){
            return Request::where('receiver_id',$this->id)->where('type','report');
        }else{
            return Request::where('sender_id',$this->id)->where('type','report');
        }
    }

    public function progressed_challenges()
    {
        return $this->hasMany(ChallengeProgress::class);
    }

}
