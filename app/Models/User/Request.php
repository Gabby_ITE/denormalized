<?php

namespace App\Models\User;

use App\Models\Book\Book;
use App\Models\User;
use App\Models\Club\Club;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    use HasFactory;

    protected $fillable = [
        'sender_id',
        'receiver_id',
        'type',
        'club_id',
        'reported_id',
        'reported_type',
        'book_id',
        'is_read',
    ];

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }
    public function receiver()
    {
        return $this->belongsTo(User::class,'receiver_id');
    }

    public function club()
    {
        return $this->belongsTo(Club::class);
    }

    public function response()
    {
        return $this->hasOne(Response::class);
    }

    public function book()
    {
        return $this->belongsTo(Book::class,'book_id');
    }
    public function reported()
    {
        if ($this->reported_type == 'series_review'){
            return Review::where('type','series')->where('id',$this->reported_id)->first();
        }
        elseif ($this->reported_type == 'book_review'){
            return Review::where('type','book')->where('id',$this->reported_id)->first();
        }
        elseif ($this->reported_type == 'book_discussion'){
            return Discussion::where('type','book')->where('id',$this->reported_id)->first();
        }
        elseif ($this->reported_type == 'series_discussion'){
            return Discussion::where('type','series')->where('id',$this->reported_id)->first();
        }
        elseif ($this->reported_type == 'club_post'){
            return Post::where('type','club')->where('id',$this->reported_id)->first();
        }
        else{
            return Post::where('type','user')->where('id',$this->reported_id)->first();
        }
    }
}
