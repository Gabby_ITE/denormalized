<?php

namespace App\Models\User;

use App\Models\Club\Club;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'post_id',
        'club_id',
        'type',
        'text',
        'images',
    ];

    protected $casts = [
        'images' =>'array',
    ];

    public function reacts()
    {
        $reacts =null;
        if ($this->type == 'user'){
            $reacts = React::where('type','user_post')->where('reacted_id',$this->id)->get();
        }else{
            $reacts = React::where('type','club_post')->where('reacted_id',$this->id)->get();
        }
        $one = collect();
        $two = collect();
        $three = collect();
        $four = collect();
        $five = collect();
        $six = collect();
        $all = collect();
        foreach($reacts as $react){
            if ($react->react == 'smile'){
                $one->add($react->react);
            }
            elseif ($react->react == 'angry'){
                $two->add($react->react);
            }
            elseif ($react->react == 'like'){
                $three->add($react->react);
            }
            elseif ($react->react == 'hot'){
                $four->add($react->react);
            }elseif ($react->react == 'cry'){
                $five->add($react->react);
            }else{
                $six->add($react->react);
            }
        }
        $all->add(['smile'=>$one->count()]);
        $all->add(['angry'=>$two->count()]);
        $all->add(['like'=>$three->count()]);
        $all->add(['hot'=>$four->count()]);
        $all->add(['cry'=>$five->count()]);
        $all->add(['love'=>$six->count()]);

        return $all;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
    public function replies()
    {
        return $this->hasMany(Post::class);
    }
    public function club()
    {
        return $this->belongsTo(Club::class);
    }
}
