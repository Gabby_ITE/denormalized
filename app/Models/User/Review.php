<?php

namespace App\Models\User;

use App\Models\User;
use App\Models\Book\Book;
use App\Models\Series;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'reviewed_id',
        'type',
        'text',
        'is_burn',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function rating()
    {
        return Rating::where('type',$this->type)->where('rated_id',$this->reviewed_id)
            ->where('user_id',$this->user_id)->first();
    }

    public function reacts()
    {
        $reacts =null;
        if ($this->type == 'book'){
            $reacts = React::where('type','book_review')->where('reacted_id',$this->id)->get();
        }else{
            $reacts = React::where('type','series_review')->where('reacted_id',$this->id)->get();
        }
        $one = collect();
        $two = collect();
        $three = collect();
        $four = collect();
        $five = collect();
        $six = collect();
        $all = collect();

        foreach($reacts as $react){
            if ($react->react == 'smile'){
                $one->add($react->react);
            }
            elseif ($react->react == 'angry'){
                $two->add($react->react);
            }
            elseif ($react->react == 'like'){
                $three->add($react->react);
            }
            elseif ($react->react == 'hot'){
                $four->add($react->react);
            }elseif ($react->react == 'cry'){
                $five->add($react->react);
            }else{
                $six->add($react->react);
            }
        }
        $all->add($one->count());
        $all->add($two->count());
        $all->add($three->count());
        $all->add($four->count());
        $all->add($five->count());
        $all->add($six->count());

        return $all;
    }

    public function reviewed()
    {
        if ($this->type == 'book'){
            return $this->belongsTo(Book::class,'reviewed_id');
        }else{
            return $this->belongsTo(Series::class,'reviewed_id');
        }
    }
}
