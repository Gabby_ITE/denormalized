<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'discussion_id',
        'discussed_id',
        'type',
        'text',
    ];

    public function reacts()
    {
        $reacts =null;
        if ($this->type == 'book'){
            $reacts = React::where('type','book_discussion')->where('reacted_id',$this->id)->get();
        }else{
            $reacts = React::where('type','series_discussion')->where('reacted_id',$this->id)->get();
        }
        $one = collect();
        $two = collect();
        $three = collect();
        $four = collect();
        $five = collect();
        $six = collect();
        $all = collect();

        foreach($reacts as $react){
            if ($react->react == 'smile'){
                $one->add($react->react);
            }
            elseif ($react->react == 'angry'){
                $two->add($react->react);
            }
            elseif ($react->react == 'like'){
                $three->add($react->react);
            }
            elseif ($react->react == 'hot'){
                $four->add($react->react);
            }elseif ($react->react == 'cry'){
                $five->add($react->react);
            }else{
                $six->add($react->react);
            }
        }
        $all->add(['smile'=>$one->count()]);
        $all->add(['angry'=>$two->count()]);
        $all->add(['like'=>$three->count()]);
        $all->add(['hot'=>$four->count()]);
        $all->add(['cry'=>$five->count()]);
        $all->add(['love'=>$six->count()]);

        return $all;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function discussed(){
        if ($this->type ==  'book'){
            return $this->belongsTo(Book::class,'discussed_id');
        }else{
            return $this->belongsTo(Series::class,'discussed_id');
        }
    }
    public function discussion()
    {
        return $this->belongsTo(Discussion::class,'discussion_id');
    }
}
