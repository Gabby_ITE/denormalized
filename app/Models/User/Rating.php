<?php

namespace App\Models\User;

use App\Models\User;
use App\Models\Author;
use App\Models\Book\Book;
use App\Models\Club\Club;
use App\Models\Series;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'rated_id',
        'type',
        'rating',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
