<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class React extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'reacted_id',
        'type',
        'react',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function reacted()
    {
        if ($this->reacted_type == 'series_review'){
            return Review::where('type','series')->where('id',$this->reacted_id)->first();
        }
        elseif ($this->reacted_type == 'book_review'){
            return Review::where('type','book')->where('id',$this->reacted_id)->first();
        }
        elseif ($this->reacted_type == 'book_discussion'){
            return Discussion::where('type','book')->where('id',$this->reacted_id)->first();
        }
        elseif ($this->reacted_type == 'series_discussion'){
            return Discussion::where('type','series')->where('id',$this->reacted_id)->first();
        }
        elseif ($this->reacted_type == 'club_post'){
            return Post::where('type','club')->where('id',$this->reacted_id)->first();
        }
        else{
            return Post::where('type','user')->where('id',$this->reacted_id)->first();
        }
    }

}
