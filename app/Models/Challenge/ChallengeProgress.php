<?php

namespace App\Models\Challenge;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChallengeProgress extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'challenge_id',
        'acheived_number',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function challenge()
    {
        return $this->belongsTo(Challenge::class);
    }
    public function scopeWithType($query, $type)
    {
        return $query->join('challenges', 'challenges.id', '=', 'challenge_progress.challenge_id')
            ->where('challenges.type', $type);
    }

    public function updateStatus()
    {
        $challenge = $this->challenge;
        if ($challenge->max_number > $this->acheived_number){
            if ($challenge->end_date < now()){
                $this->update(['status'=>'timeout']);
            }
        }else{
            if ($challenge->end_date >= now()){
                $this->update(['status'=>'finished']);
            }
        }
    }
}

