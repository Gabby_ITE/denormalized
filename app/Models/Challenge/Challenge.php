<?php

namespace App\Models\Challenge;

use App\Models\Book\Book;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Challenge extends Model
{
    use HasFactory;

    protected $fillable = [
        'creator_id',
        'book_id',
        'name',
        'is_private',
        'start_date',
        'end_date',
        'type',
        'max_number',
    ];

    public function scopeBookType($query)
    {
        $query->where('type','books');
    }
    public function scopePageType($query)
    {
        $query->where('type','pages');
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }
    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    public function progresses()
    {
        return $this->hasMany(ChallengeProgress::class);
    }

}
