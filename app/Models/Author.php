<?php

namespace App\Models;

use App\Models\Book\Book;
use App\Models\User\Rating;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'image',
        'books',
        'series',
    ];

    protected $casts = [
        'books' =>'array',
        'series' =>'array',
    ];
    public function books(){
        $books = collect();
        foreach($this->books as $bookId){
            $book = Book::find($bookId);
            $books->add($book);
        }
        return $books;
    }

    public function series(){
        $series = collect();
        foreach($this->series as $serId){
            $ser = Series::find($serId);
            $series->add($ser);
        }
        return $series;
    }


    public function full_ratings()
    {
        $ratings = Rating::where('type','author')->where('rated_id',$this->id)->get();
        $oneStar = collect();
        $twoStar = collect();
        $threeStar = collect();
        $fourStar = collect();
        $fiveStar = collect();
        $allStars = collect();

        foreach($ratings as $rate){
            if ($rate->rating == 1){
                $oneStar->add($rate->rating);
            }
            elseif ($rate->rating == 2){
                $twoStar->add($rate->rating);
            }
            elseif ($rate->rating == 3){
                $threeStar->add($rate->rating);
            }
            elseif ($rate->rating == 4){
                $fourStar->add($rate->rating);
            }else{
                $fiveStar->add($rate->rating);
            }
        }
        $allStars->add($oneStar->count());
        $allStars->add($twoStar->count());
        $allStars->add($threeStar->count());
        $allStars->add($fourStar->count());
        $allStars->add($fiveStar->count());

        return $allStars;
    }

    public function ratings()
    {
        $ratings = Rating::where('type','author')->where('rated_id',$this->id)->get();
        $rate = collect();
        foreach($ratings as $r){
            $rate->add($r->rating);
        }
        return $rate->average();
    }
}
