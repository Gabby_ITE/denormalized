<?php

namespace App\Models\Book;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookNote extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'book_id',
        'note',
        'color',
        'page_number',
    ];
}
