<?php

namespace App\Models;

use App\Models\Book\Book;

use App\Models\Club\Club;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'books',
        'series',
        'clubs',
    ];

    protected $casts = [
        'books' =>'array',
        'series' =>'array',
        'clubs'=>'array',
    ];
    public function series()
    {
        $series = collect();
        foreach($this->series as $serieId){
            $serie = Series::find($serieId);
            $series->add($serie);
        }
        return $series;
    }

    public function clubs()
    {
        $clubs = collect();
        foreach($this->clubs as $clubId){
            $club = Club::find($clubId);
            $clubs->add($club);
        }
        return $clubs;
    }

    public function books(){
        $books = collect();
        foreach($this->books as $bookId){
            $book = Book::find($bookId);
            $books->add($book);
        }
        return $books;
    }

}
