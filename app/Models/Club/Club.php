<?php

namespace App\Models\Club;

use App\Models\Tag;
use App\Models\User\Rating;
use App\Models\User;
use App\Models\User\Post;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    use HasFactory;

    protected $fillable = [
        'admin_id',
        'name',
        'image',
        'description',
        'is_private',
        'is_closed',
        'tags',
    ];

    protected $casts = [
        'tags' =>'array',
    ];

    public function admin()
    {
        return $this->belongsTo(User::class,'admin_id');
    }
    public function tags()
    {
        $tags = collect();
        foreach ($this->tags as $tagId){
            $tag = Tag::find($tagId);
            $tags->add($tag);
        }
        return $tags;
    }
    public function members()
    {
        return $this->hasMany(ClubMember::class);
    }

    public function posts()
    {
        return Post::where('type','club')->where('club_id',$this->id)->get();
    }

    public function full_ratings()
    {
        $ratings = Rating::where('type','club')->where('rated_id',$this->id)->get();
        $oneStar = collect();
        $twoStar = collect();
        $threeStar = collect();
        $fourStar = collect();
        $fiveStar = collect();
        $allStars = collect();

        foreach($ratings as $rate){
            if ($rate->rating == 1){
                $oneStar->add($rate->rating);
            }
            elseif ($rate->rating == 2){
                $twoStar->add($rate->rating);
            }
            elseif ($rate->rating == 3){
                $threeStar->add($rate->rating);
            }
            elseif ($rate->rating == 4){
                $fourStar->add($rate->rating);
            }else{
                $fiveStar->add($rate->rating);
            }
        }
        $allStars->add($oneStar);
        $allStars->add($twoStar);
        $allStars->add($threeStar);
        $allStars->add($fourStar);
        $allStars->add($fiveStar);

        return $allStars;
    }

    public function hasTag($id)
    {
        foreach($this->tags as $tag){
            if ($tag == $id)
                return true;
        }
        return false;
    }
    public function ratings()
    {
        $ratings = Rating::where('type','club')->where('rated_id',$this->id)->get();
        $rate = collect();
        foreach($ratings as $r){
            $rate->add($r->rating);
        }
        return $rate->average();
    }

}
