<?php

namespace App\Models;

use App\Models\Book\Book;
use App\Models\Combine\Description;
use App\Models\User\Discussion;
use App\Models\User\Rating;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'cover',
        'release_date',
        'tags',
        'authors',
    ];

    protected $casts = [
        'authors' =>'array',
        'tags' =>'array',
    ];

    public function hasTag($id)
    {
        foreach($this->tags as $tag){
            if ($tag == $id)
                return true;
        }
        return false;
    }
    public function hasAuthor($id)
    {
        foreach($this->authors as $author){
            if ($author == $id)
                return true;
        }
        return false;
    }
    public function tags()
    {
        $tags = collect();
        foreach($this->tags as $tagId){
            $tag = Tag::find($tagId);
            $tags->add($tag);
        }
        return $tags;
    }

    public function authors()
    {
        $authors = collect();
        foreach($this->authors as $authorId){
            $author = Author::find($authorId);
            $authors->add($author);
        }
        return $authors;
    }

    public function ratings()
    {
        $ratings = Rating::where('type','series')->where('rated_id',$this->id)->get();
        $rate = collect();
        foreach($ratings as $r){
            $rate->add($r->rating);
        }
        return $rate->average();
    }

    public function full_ratings()
    {
        $ratings = Rating::where('type','series')->where('rated_id',$this->id)->get();
        $oneStar = collect();
        $twoStar = collect();
        $threeStar = collect();
        $fourStar = collect();
        $fiveStar = collect();
        $allStars = collect();

        foreach($ratings as $rate){
            if ($rate->rating == 1){
                $oneStar->add($rate->rating);
            }
            elseif ($rate->rating == 2){
                $twoStar->add($rate->rating);
            }
            elseif ($rate->rating == 3){
                $threeStar->add($rate->rating);
            }
            elseif ($rate->rating == 4){
                $fourStar->add($rate->rating);
            }else{
                $fiveStar->add($rate->rating);
            }
        }
        $allStars->add($oneStar);
        $allStars->add($twoStar);
        $allStars->add($threeStar);
        $allStars->add($fourStar);
        $allStars->add($fiveStar);

        return $allStars;
    }

    public function discussions()
    {
        $discussions = Discussion::where('type','series')->where('discussed_id',$this->id)->get();
        return $discussions;
    }
    public function descriptions()
    {
        $descriptions = Description::where('type','series')->where('described_id',$this->id)->get();
        return $descriptions;
    }

    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
