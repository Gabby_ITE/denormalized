<?php

namespace App\Models\Combine;

use App\Models\Book\Book;
use App\Models\Series;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Description extends Model
{
    use HasFactory;
    protected $fillable = [
        'described_id',
        'type',
        'name',
        'text',
    ];

    public function descriped()
    {
        if ($this->type ==  'book'){
            return $this->belongsTo(Book::class,'described_id');
        }else{
            return $this->belongsTo(Series::class,'described_id');
        }
    }
}
